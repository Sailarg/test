<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Queue Driver
    |--------------------------------------------------------------------------
    |
    | The Laravel queue API supports a variety of back-ends via an unified
    | API, giving you convenient access to each back-end using the same
    | syntax for each one. Here you may set the default queue driver.
    |
<<<<<<< HEAD
    | Supported: "null", "sync", "database", "beanstalkd", "sqs", "redis"
=======
    | Supported: "sync", "database", "beanstalkd", "sqs", "redis", "null"
>>>>>>> c3848a4a2a397e7952f930f05633d2a9c6ddb14b
    |
    */

    'default' => env('QUEUE_DRIVER', 'sync'),

    /*
    |--------------------------------------------------------------------------
    | Queue Connections
    |--------------------------------------------------------------------------
    |
    | Here you may configure the connection information for each server that
    | is used by your application. A default configuration has been added
    | for each back-end shipped with Laravel. You are free to add more.
    |
    */

    'connections' => [

        'sync' => [
            'driver' => 'sync',
        ],

        'database' => [
            'driver' => 'database',
            'table' => 'jobs',
            'queue' => 'default',
<<<<<<< HEAD
            'expire' => 60,
=======
            'retry_after' => 90,
>>>>>>> c3848a4a2a397e7952f930f05633d2a9c6ddb14b
        ],

        'beanstalkd' => [
            'driver' => 'beanstalkd',
            'host' => 'localhost',
            'queue' => 'default',
<<<<<<< HEAD
            'ttr' => 60,
=======
            'retry_after' => 90,
>>>>>>> c3848a4a2a397e7952f930f05633d2a9c6ddb14b
        ],

        'sqs' => [
            'driver' => 'sqs',
            'key' => 'your-public-key',
            'secret' => 'your-secret-key',
            'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
            'queue' => 'your-queue-name',
            'region' => 'us-east-1',
        ],

        'redis' => [
            'driver' => 'redis',
            'connection' => 'default',
            'queue' => 'default',
<<<<<<< HEAD
            'expire' => 60,
=======
            'retry_after' => 90,
>>>>>>> c3848a4a2a397e7952f930f05633d2a9c6ddb14b
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Failed Queue Jobs
    |--------------------------------------------------------------------------
    |
    | These options configure the behavior of failed queue job logging so you
    | can control which database and table are used to store the jobs that
    | have failed. You may change them to any database / table you wish.
    |
    */

    'failed' => [
        'database' => env('DB_CONNECTION', 'mysql'),
        'table' => 'failed_jobs',
    ],

];
