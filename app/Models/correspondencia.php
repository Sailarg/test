<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class correspondencia extends Model{
    
	protected $table = "correspondencia";
	
	protected $fillable = [
        'correspondencia_dia', 'correspondencia_ced'
    ];
	
	protected $hidden = [
        'user_id'
    ];
	
	public function users(){
        return $this->hasOne('App\Models\Users','user_id','id');
    }
}
