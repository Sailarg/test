<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class carrera extends Model {

	protected $table = 'carrera';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'codigo','menciones', 'materias_importantes', 'facultad_id', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getListWhereIn($data)
    {
        return carrera::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListNameWhereIn($data)
    {
        return carrera::select('nombre')
            ->whereIn('codigo', $data);
    }
    public function getListCodigoWhereIn($data)
    {
        return carrera::select('codigo')->orderBy("nombre", "asc")
            ->whereIn('nombre', $data);
    }

    public function getListWhereFacultadIdIn($data)
    {
        return carrera::select('nombre')->orderBy("nombre", "asc")
            ->whereIn('facultad_id', $data);
    }

    public function getListWhereFacultadIdIn2($data,$menc)
    {
        return carrera::select('nombre')->orderBy("nombre", "asc")->where('menciones', 'LIKE', '%'. $menc .'%')
            ->whereIn('facultad_id', $data);
    }


    public function getListWhereEspecialidadIdIn($data)
    {
        return carrera::select('facultad_id','nombre','codigo')->orderBy("nombre", "asc")
            ->where('menciones', 'LIKE', '%'. $data .'%');
    }

    public function getListWhereNotIn($data)
    {
        return carrera::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return carrera::where('nombre', 'LIKE', '%'. $find .'%');
    }


}