<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class area extends Model {

	protected $table = 'area';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getFacultad()
    {
        return facultad::where('area_id',$this->id)->first()['nombre'];
    }

    public function getCarrera()
    {
        $facultad_id = facultad::where('area_id',$this->id)->first()['id'];
        return carrera::where('facultad_id', $facultad_id)->first()['nombre'];
    }

    public function getListWhereIn($data)
    {
        return area::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListWhereNotIn($data)
    {
        return area::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return area::where('nombre', 'LIKE', '%'. $find .'%');
    }



}