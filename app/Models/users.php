<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Hash;
use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;


class users extends Model {

    private $destinationPath = '\\archivements\\pdfs\\';

    protected $table = "user";

	protected $fillable = [
        'user_name', 'user_apellido', 'user_cedula',
		'user_liceo', 'user_genero', 'user_especialidad',
		'user_status', 'user_email', 'user_password','user_fecha_nacimiento',
		'user_estado', 'user_municipio', 'user_parroquia',
		'user_phone', 'user_liceo_estado', 'user_liceo_municipio',
        'user_liceo_parroquia', 'user_pdf', 'user_opciones'
    ];

	protected $hidden = [
        'api_token', 'api_token_estatus', 'user_password', 'user_status', 'user_paso',
        'user_notas', 'user_promedio', 'user_promedio2', 'user_promedio3',
    ];

    /*
        user_status
        10 cuenta inactiva
        20 cuenta bloqueada
        30 cuenta activa
    */

	public function correspondencia()
    {
		return $this->belongsTo('App\Models\Correspondencia');
	}

    public function usersExist($data)
    {
        $user = users::where("user_cedula", $data["cedula"])
            ->first();

        if(sizeof($user) > 0)
        {
            return 0;
        }

        $user = users::where("user_email", $data["correo"])
            ->first();

        if(sizeof($user) > 0)
        {
            return 1;
        }

        return 2;
    }

    public function getUserByCI($ced)
    {
        return users::where("user_cedula", $ced)->first();
    }

    public function getUserByEmail($email)
    {
        return users::where("user_email", $email)->first();
    }

    public function usersCanLogin($data)
    {
        $user = users::where("user_cedula", $data['cedula'])
                ->first();

        if(sizeof($user) > 0)
        {
            $hash = hash('ripemd320',$data['password']);

            if( $hash == $user["user_password"])
            {
                if($user->user_status == '30')
                {
                    if($user->user_paso >= 1 && $user->user_paso <= 4)
                    {
                        Session::put("login", true);
                        Session::put("ci", $user["user_cedula"]);
                        Session::put("nombre", $user["user_name"]);
                        Session::put("activo_user", Collection::make($user));
                        return 0;
                    }

                    return 1;
                }

                elseif($user->user_status == '20')
                {
                    return 2;
                }

                elseif($user->user_status == '10')
                {
                    return 3;
                }

            }

            return 4;

        }

        return 5;

    }

    public function hasFile()
    {
        if($this->user_pdf != ''  || $this->user_pdf != null)
        {
            return true;
        }

        return false;
    }

    public function deteteFile()
    {
        File::delete(storage_path() . $this->destinationPath . $this->user_pdf);
        $this->updateScore(['ci' => $this->user_cedula, 'score' => '', 'fileName' => '']);
    }

    public function usersCanActivate($data)
    {
        $user = users::where('api_token', $data)
                 ->where('api_token_estatus',0)
                 ->first();

        if(sizeof($user) > 0)//el token no ha sido usado
        {
            $this->activate(['ci' => $user["user_cedula"]]);
            return 1;
        }

        return 0;
    }

    private function activate($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [ "api_token_estatus" => 1,
                  "user_status" => 30
                ]);
    }

    public function createUser($data)
    {
        $usuario = new users([
            'user_name' => $data["nombre"],
            'user_apellido' => $data["apellido"],
            'user_cedula' => $data["cedula"],
            'user_phone' => $data["telefono"],
            'user_email' => $data["correo"],
            'user_genero' => $data["genero"],
            'user_fecha_nacimiento' => $data["fecha_nacimiento"],
            'user_especialidad' => $data["especialidad"],
            'user_estado' => $data["estado"],
            'user_municipio' => $data["municipio"],
            'user_parroquia' => $data["parroquia"],
            'user_liceo' => $data["liceo"],
            'user_liceo_estado' => $data["user_liceo_estado"],
            'user_liceo_municipio' => $data["user_liceo_municipio"],
            'user_liceo_parroquia' => $data["user_liceo_parroquia"],
        ]);
            $pass = str_random(16);
            $usuario->user_password = hash('ripemd320', $pass);
            $usuario->user_status = "10";
            $usuario->api_token = str_random(255);
            $usuario->user_paso = 1;
        $usuario->save();

        return array('token' => $usuario->api_token, 'clave' => $pass);
    }

	public function editUser($data)
	{
        \DB::table('user')
            ->where("user_cedula", $data["ci"])
            ->update(
                [
                    'user_name' => $data["nombre"],
                    'user_apellido' => $data["apellido"],
                    'user_phone' => $data["telefono"],
                    'user_genero' => $data["genero"],
                    'user_especialidad' => $data["especialidad"],
                    'user_estado' => $data["estado"],
                    'user_municipio' => $data["municipio"],
                    'user_parroquia' => $data["parroquia"],
                    'user_liceo' => $data["liceo"],
                    'user_liceo_estado' => $data["user_liceo_estado"],
                    'user_liceo_municipio' => $data["user_liceo_municipio"],
                    'user_liceo_parroquia' => $data["user_liceo_parroquia"],
                ]);
	}

    public function changePassword($data)
    {
        $change = \DB::table('user')
            ->where("user_cedula", $data["ci"])
            ->where("user_password", hash('ripemd320', $data['oldpass']))
            ->first();

        if(sizeof($change) > 0)
        {
            \DB::table('user')
                ->where("user_cedula", $data["ci"])
                ->update(
                    [
                        "user_password" => hash('ripemd320', $data['pass']),
                    ]);
            return true;
        }
        return false;
    }

    public function renewPasswordUser($data)
    {
        $pass = str_random(16);
        $token = str_random(255);
        \DB::table('user')
            ->where("user_cedula", $data["ci"])
            ->update(
                [
                    "user_password" => hash('ripemd320', $pass),
                    "api_token" => $token,
                    "api_token_estatus" => 0,
                    'user_status' => 20,
                ]);

        return array('clave' => $pass, 'code' => $token);
    }

    public function updateStep($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [
                    "user_paso" => $data["paso"],
                ]);
    }

    public function updateOpciones($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [
                    "user_opciones" => $data["opciones"],
                ]);
    }

    public function updateNotas($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [
                    "user_notas" => $data["score"]
                ]);
    }

    public function updateScore($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [
                    "user_notas" => $data["score"],
                    "user_pdf" => $data["fileName"],
                ]);
    }

    public function updateAverage($data)
    {
        \DB::table('user')
            ->where("user_cedula",$data["ci"])
            ->update(
                [
                    "user_promedio" => $data["p1"],
                    "user_promedio2" => $data["p2"],
                    "user_promedio3" => $data["p3"],
                ]);
    }

}