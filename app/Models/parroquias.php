<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class parroquias extends Model {

	protected $table = 'parroquia';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'municipio_id', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getListWhereIn($data)
    {
        return parroquias::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListWhereNotIn($data)
    {
        return parroquias::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return parroquias::where('nombre', 'LIKE', '%'. $find .'%');
    }

    public function getListWhereMunicipioIdIn($data)
    {
        return parroquias::select('nombre')->orderBy("nombre", "asc")
            ->where('municipio_id', $data);
    }

}