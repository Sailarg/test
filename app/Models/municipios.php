<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class municipios extends Model {

	protected $table = 'municipio';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'estado_id', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getListWhereIn($data)
    {
        return municipios::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListWhereNotIn($data)
    {
        return municipios::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return municipios::where('nombre', 'LIKE', '%'. $find .'%');
    }

    public function getListWhereEstadoIdIn($data)
    {
        return municipios::orderBy("nombre", "asc")
            ->where('estado_id', $data);
    }

}