<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class materias extends Model {

    protected $table = 'materia_liceo';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['codigo','nombre','created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getAllMaterias(){

            return materias::select('codigo', 'nombre')->orderBy("nombre", "asc")->get();
    }


}