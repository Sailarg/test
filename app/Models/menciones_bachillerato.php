<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class menciones_bachillerato extends Model {

	protected $table = 'menciones_bachillerato';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'codigo', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getListWhereIn($data)
    {
        return menciones_bachillerato::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListWhereNotIn($data)
    {
        return menciones_bachillerato::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return menciones_bachillerato::where('nombre', 'LIKE', '%'. $find .'%');
    }

}