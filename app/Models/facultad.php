<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class facultad extends Model {

	protected $table = 'facultad';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = ['nombre', 'area_id', 'created_by', 'updated_by'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */

    protected $hidden = ['id','created_at','updated_at'];

    public $timestamps = true;

    public function createdAt()
    {
        return  date('m-d-Y', strtotime($this->created_at));
    }

    public function updatedAt()
    {
        return  date('m-d-Y', strtotime($this->updated_at));
    }

    public function getListWhereIn($data)
    {
        return facultad::orderBy("nombre", "asc")
            ->whereIn('id', $data);
    }

    public function getListWhereAreaByFacultad($data)
    {
        return facultad::select('area_id', 'nombre')
            ->where('id', $data);
    }

    public function getListWhereAreaIdIn($data)
    {
        return facultad::select('nombre')->orderBy("nombre", "asc")
            ->whereIn('area_id', $data);
    }

    public function getListWhereAreaIdIn2($data)
    {
        return facultad::orderBy("nombre", "asc")
            ->whereIn('area_id', $data);
    }

    public function getListWhereNotIn($data)
    {
        return facultad::orderBy("nombre", "asc")
            ->whereNotIn('id', $data);
    }

    public function search($find)
    {
        return facultad::where('nombre', 'LIKE', '%'. $find .'%');
    }



}