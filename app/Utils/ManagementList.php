<?php

namespace App\Utils;
use App\Models\area;
use App\Models\carrera;
use App\Models\estados;
use App\Models\facultad;
use App\Models\municipios;
use App\Models\parroquias;
use App\Models\users;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Session;

/**
 * Created by PhpStorm.
 * User: Slei
 * Date: 09-12-2015
 * Time: 05:53 PM
 */


class ManagementList
{
    public static function processListName($lista)
    {
        $result = array('Lista');
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->nombre;
        }
        return $result;
    }

    public static function processListNameCodigo($lista)
    {
        $result = array('Lista');
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->codigo] = $lista[$i]->nombre;
        }
        return $result;
    }

    public static function processListTipo($lista)
    {
        $result = array('Lista');
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->tipo;
        }

        return $result;
    }

    public static function processListName2($lista, $name)
    {
        $result[0] = $name;
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->nombre;
        }

        return $result;

    }

    public static function processListDescripcion($lista)
    {
        $result = array('Lista');
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->descripcion;
        }

        return $result;
    }

    public static function processListArea($lista)
    {
        $result = array('Lista');
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->nombre;
        }

        return $result;
    }

    public static function processListId($lista)
    {
        $result = [];
        for($i = 0; $i < sizeof($lista); $i++)
        {
            $result[$lista[$i]->id] = $lista[$i]->id;
        }

        return $result;
    }

    public static function getAreaJson()
    {
        $ci = Session::get('ci');
        $user = new users();
        $usuario = $user->getUserByCI($ci);

        $area = new area();
        $areas = $area->getListWhereNotIn([])->get();

        $facultad = new facultad();
        $carrera = new carrera();

        for($i = 0; $i < sizeof($areas); $i++)
        {
            $facultades = $facultad->getListWhereAreaIdIn([$areas[$i]->id])->get();
            $facultades_ids = $facultad->getListWhereAreaIdIn2([$areas[$i]->id])->get();
            for($j = 0; $j < sizeof($facultades); $j++)
            {
                $facultades[$j]["carrera"] = $carrera->getListWhereFacultadIdIn([$facultades_ids[$j]->id])->get();
            }
            $areas[$i]["facultad"] = $facultades;
        }

        $json = json_encode($areas);
        return $json;
    }

    public static function getUbicacionGeografica()
    {
        $estados = new estados();
        $municipio = new municipios();
        $parroquias = new parroquias();

        $lista_estados = $estados->getListWhereIn([24])->get();

        for($i = 0; $i < sizeof($lista_estados); $i++)
        {
            $municipios = $municipio->getListWhereEstadoIdIn([$lista_estados[$i]->id])->get();
            for($j = 0; $j < sizeof($municipios); $j++)
            {
                $municipios[$j]["parroquia"] = $parroquias->getListWhereMunicipioIdIn([$municipios[$j]->id])->get();

            }

            $lista_estados[$i]["municipio"] = $municipios;
        }

        return \DB::table('ubicacion_geografica')->select('lista')->first();
    }

}