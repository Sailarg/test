<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\users;
use Redirect;
use Crypt;
use Hash;
use Session;

class PanelController extends Controller{
    	
	public function salir()
	{
		Session::flush();
		return Redirect::to('/');
	}
	
	public function index(LoginRequest $request)
    {
dd('d');    
   $user = new users();
        $data = array(
            'password' => $request["pwd"],
            'cedula' => $request["ced"],
        );

        switch($user->UsersCanLogin($data))
        {
            case 0:
                return Redirect::route("candidate/index");

            case 1:
                Session::flush();
                return redirect()->back()
                    ->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "Debe esperar el dia de la prueba",
                ]);

            case 2:
                Session::flush();
                return redirect()->back()
                    ->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "Por su seguridad la cuenta ha sido bloqueda.",
                ]);

            case 3:
                Session::flush();
                return redirect()->back()
                    ->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "La cuenta no ha sido activada",
                ]);

            case 4:
                Session::flush();
                return redirect()->back()
                    ->with([
                        "confirmar_error" => "alert-danger",
                        "message" => "Datos incorrectos, intente nuevamente",
                    ]);

            case 5:
                Session::flush();
                return redirect()->back()
                    ->with([
                        "confirmar_error" => "alert-danger",
                        "message" => "Esta cuenta no existe"
                    ]);

            default:
                Session::flush();
                return redirect()->back()
                    ->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "Ocurrio un error, por favor intente nuevamente",
                ]);

		}
	}

    public function getConfirm()
    {
        return Redirect::to('/');
    }

    public function postConfirm($token)
    {
        $user = new users();
        $result = $user->usersCanActivate($token);

        switch($result)
        {
            case 1:
                return Redirect::to('/')
                    ->with([
                            "confirmar_error" => "alert-success",
                            "message" => "Cuenta activada"
                    ]);

            case 0:
                return Redirect::to('/')
                    ->with([
                           "confirmar_error" => "alert-danger",
                            "message" => "La cuenta no pudo ser activada"
                    ]);
        }
    }

}
