<?php

namespace App\Http\Controllers;

use App\Http\Requests\AspiranteRequest;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Session;
use Redirect;

class AdminController extends Controller
{
    private $role = 100;

    public function index()
    {
        return view('backend.index', ['role' => $this->role]);
    }

    public function profile()
    {
        return view('backend.profile',['role' => $this->role]);
    }

}
