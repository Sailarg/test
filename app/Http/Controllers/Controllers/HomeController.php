<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Collection as Collection;
use App\Models\correspondencia;
use Illuminate\Support\Facades\Redirect;
use App\Models\menciones_bachillerato;
use App\Utils\ManagementList;
use Session;
use App;
use DB;

class HomeController extends Controller{


    public function getIndex()
    {
        return Redirect::to('/');
    }

	public function index()
	{
       Session::set("size_cedula",Correspondencia::count());
		if(!Session::has("confirmar_error"))
            Session::set("confirmar_error","alert-warning");
		Session::set("configurar_cedula",Collection::make(Correspondencia::select("correspondencia_dia","correspondencia_ced")->get())->tojson());
        return view('index');
	}

    public function create()
    {
        $mencion = new menciones_bachillerato();
        $menciones = ManagementList::processListNameCodigo($mencion->getListWhereNotIn([])->get());
        $ubicaciones = ManagementList::getUbicacionGeografica();

        return view('account', ['menciones' => $menciones]);
    }
}
