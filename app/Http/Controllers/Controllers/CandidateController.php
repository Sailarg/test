<?php

namespace App\Http\Controllers;

use App\Http\Requests\BlankRequest;
use App\Http\Requests\CandidateRequest;
use App\Http\Requests\ChangePasswordRequest;
use Illuminate\Http\Request;
use App\Models\carrera;
use App\Models\users;
use App\Models\materias;
use App\Utils\ManagementList;
use App\Utils\pdf2text;
use App\Models\menciones_bachillerato;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;
use Crypt;
use Mail;
use Illuminate\Support\Collection as Collection;

class CandidateController extends Controller
{
    private $role = 25;
	private $confirmar = false;

    public function store(CandidateRequest $request)
    {
        $usuario = new users();
        $exist = $usuario->usersExist(['cedula' => $request["cedula"], 'correo' => $request["correo"]]);

        switch($exist)
        {
            case 0:
                return redirect()->back()->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "La cedula suministrada ya esta registrada"
                ]);
                break;

            case 1:
                return redirect()->back()->with([
                    "confirmar_error" => "alert-danger",
                    "message" => "El correo suministrado ya se encuentra registrado"
                ]);
                break;

            case 2:
                continue;
                break;
        }

        $user = $usuario->createUser(
            [
                "nombre" => $request["nombre"],
                "apellido" =>  $request["apellido"],
                "cedula" =>  $request["cedula"],
                "telefono" =>  $request["codigo"]."-".$request["telefono"],
                "fecha_nacimiento" => $request["fecha_nacimiento"],
                "genero" =>  $request["genero"],
                "correo" =>  $request["correo"],
                "liceo" =>  $request["liceo"],
                "especialidad" =>  $request["especialidad"],
                "estado" =>  $request["estado"],
                "municipio" =>  $request["municipio"],
                "parroquia" =>  $request["parroquia"],
                'user_liceo_estado' => $request["liceo_estado"],
                'user_liceo_municipio' => $request["liceo_municipio"],
                'user_liceo_parroquia' => $request["liceo_parroquia"],
            ]
        );

        $data = [
            "correo" => $request["correo"],
            "code" => $user['token'],
            "pass" => $user['clave']
        ];

        Mail::send('layout.email', $data, function ($message) use($request) {
            $message->from("cbbiancocesar@gmail.com", "SIGA LAS INSTRUCCIONES PARA ACTIVAR SU CUENTA");
            $message->to($request["correo"], "S.N.I. U.C.V.")->subject("SNI: CLAVE DE ACCESO");
        });

        return Redirect::to('/')->with([
            "confirmar_error" => "alert-success",
            "message" => "Registro realizado con éxito"
        ]);
    }

    public function edit(CandidateRequest $request)
    {
        $ced = $request["ced"];
        $user = new users();
        $usuario = $user->getUserByCI($ced);
        $user->editUser([
            "nombre" => ($request["nombre"]==""?$usuario->user_name:$request["nombre"]),
            "apellido" => ($request["apellido"]==""?$usuario->user_apellido:$request["apellido"]),
            "telefono" => ($request["telefono"]==""?$usuario->user_phone:$request["telefono"]),
            "liceo" => ($request["liceo"]==""?$usuario->user_liceo:$request["liceo"]),
            "genero" => ($request["genero"]=="-1"?$usuario->user_genero:$request["genero"]),
            "especialidad" => ($request["especialidad"]=="NULL"?$usuario->user_especialidad:$request["especialidad"]),
            "clave" => ($request["clave"]==""?$usuario->user_password:hash('ripemd320', $request["clave"]))
        ]);

        Session::set("activo_user",Collection::make($user->getUserByCI($ced)));

        return Redirect::back()->with([
            "confirmar_error" => "0",
            "message" => "Modificaci&oacute;n realizada con &eacute;xito"
        ]);
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $ced = Session::get("ci");
        $user = new users();
        $data = array('ci' => $ced, 'pass' => $request['clave'], 'oldpass' => $request['actual']);
        if($user->changePassword($data))
        {
            return redirect()->back()->with([
                "confirmar_error" => "alert-danger",
                "message" => "Cambio de clave exitoso",
            ]);
        }

        return redirect()->back()->with([
            "confirmar_error" => "alert-danger",
            "message" => "Clave actual invalida",
        ]);
    }

    public function index()
    {
        return view('backend.index', ['role' => $this->role]);
    }

    public function profile()
    {
        $ced = Session::get("ci");
        $user = new users();
        $candidate = $user->getUserByCI($ced);
        $phone = explode("-", $candidate->user_phone);
        $mencion = new menciones_bachillerato();
        $menciones = ManagementList::processListNameCodigo($mencion->getListWhereNotIn([])->get());
        return view('backend.perfil',['role' => $this->role, "candidate" => $candidate, "menciones" => $menciones,'phone' => $phone[1]]);
    }

    public function inscription()
    {
        if(Session::has("ci"))
        {
            $ced = Session::get("ci");
            $user = new users();
            $candidate = $user->getUserByCI($ced);
            switch($candidate->user_paso)
            {
                case 1:
                    $materia = new materias();
                    $materias= $materia->getAllMaterias();
                    $listamaterias =array();
                    for($i=0;$i<sizeof($materias);$i++)
                    {
                        $listamaterias[$materias[$i]->codigo]=$materias[$i]->nombre;
                    }

                    return view('backend.inscripcion',['jsonpdf' => $candidate->user_notas, 'role' => $this->role, 'materias' => $listamaterias, 'confirmar' => $this->confirmar]);

                case 2:
                    $areas = ManagementList::getAreaJson();
                    return view('backend.inscripcion2',['role' => $this->role, 'areas' => $areas]);

                case 3:
                    $phone = explode("-", $candidate->user_phone);
                    $opcion =explode(",",$candidate->user_opciones);
                    $opciones=array();
                    for($i=0;$i<sizeof($opcion);$i++)
                    {
                        $carrea = new carrera();
                        $temp= $carrea->getListNameWhereIn([$opcion[$i]])->get();
                        $opciones[$opcion[$i]]=$temp[0]['nombre'];
                    }

                    $json = $candidate->user_notas;
                    $mencion = new menciones_bachillerato();
                    $menciones = ManagementList::processListNameCodigo($mencion->getListWhereNotIn([])->get());

                    return view('backend.inscripcion3',['role' => $this->role, "candidate" => $candidate,"opciones"=>$opciones, "menciones" => $menciones,'phone' => $phone[1],"json"=> $json]);

                case 4:
                     return view('backend.index', ['role' => $this->role]);
            }
            return redirect()->to("/");
        }
    }

    public function getuploadFile(Request $request)
    {
        return $this->inscription();
    }

    public function uploadFile(Request $request)
    {
        //habilitar en el php.ini la extension para ver tipos de archivos, (extension=php_fileinfo.dll)
        $file = $request->file('file');
        if ($request->hasFile('file') && $file->getClientOriginalExtension() == 'pdf' && File::size($file) < 1000000)
        {
            $ced = Session::get("ci");
            $user = new users();
            $usuario = $user->getUserByCI($ced);
            $destinationPath = '../storage/archivements/pdfs/';
            $extension = 'pdf'; //$file->getClientOriginalExtension(); // getting image extension
            if($usuario->hasFile())
            {
                $usuario->deteteFile();
            }

            $fileName = str_random(30).'.'.$extension; // rename file
            $file->move($destinationPath, $fileName);
            $pdf = new pdf2text();
            $json = $pdf->pdftojson($destinationPath . $fileName);
            if($json)
            {
                $user->updateScore(['ci' => $ced, 'score' => $json, 'fileName' => $fileName]);
                $this->confirmar = true;
            }
            return $this->inscription();
        }
        else {
            return redirect()->back()->with([
                "confirmar_error" => "alert-danger",
                "message" => "Debe seleccionar un archivo en formato pdf",
            ]);
        }
    }

    public function postStep1(BlankRequest $request)
    {
        $user = new users();
        $ced = Session::get("ci");
        $usuario = $user->getUserByCI($ced);
        if($usuario->user_paso == 1)
        {
            $json = ($request['notasPDF']);
            $user->updateNotas(['ci' => $ced, 'score' => $json]);
            $user->updateStep(['ci' => $ced, 'paso' => 2]);
        }

        return $this->inscription();
    }

    public function postStep1Manual(BlankRequest $request)
    {
        $user = new users();
        $carrera = new carrera();
        $ced = Session::get("ci");
        $usuario = $user->getUserByCI($ced);
        if($usuario->user_paso == 1)
        {
            $json2=array();
            $json = json_decode($request['notasCargadas']);
            $json2["ano1"]=array();
            $json2["ano2"]=array();
            $json2["ano3"]=array();
            $json2["ano4"]=array();
            for($i=0;$i<sizeof($json);$i++)
            {
                for($j=0;$j<sizeof($json[$i]);$j++)
                {
                   $json2["ano".($i+1)][$json[$i][$j]->materia]=$json[$i][$j]->nota;
                }
            }
            $Jsonfinal=Collection::make($json2)->toJson();
            $user->updateScore(['ci' => $ced, 'score' => $Jsonfinal, 'fileName' => ""]);
            $user->updateStep(['ci' => $ced, 'paso' => 2]);
        }

        return $this->inscription();
    }

    public function postStep2(BlankRequest $request)
    {
        $ced = Session::get("ci");
        $user = new users();
        $usuario = $user->getUserByCI($ced);
        if($usuario->user_paso == 2)
        {

            $json = json_decode($request['seleccionar_area']);
            $cod=array();
            $cods="";
            for($i=0;$i<sizeof($json);$i++){
                $carrea = new carrera();
                $cod[] = $carrea->getListCodigoWhereIn([$json[$i]->carrera])->get();
            }

            for($i=0;$i<sizeof($json);$i++){
                if($i!=0)
                    $cods.=','.$cod[$i][0]['codigo'];
                else
                    $cods.=$cod[$i][0]['codigo'];
            }
            $user->updateOpciones(['ci' => $ced, 'opciones' => $cods]);
            $user->updateStep(['ci' => $ced, 'paso' => 3]);
            $this->sendMailresume();
        }
        return $this->inscription();
    }

    private function sendMailresume()
    {
        $ced = Session::get("ci");
        $user = new users();
        $candidate = $user->getUserByCI($ced);
        $phone = explode("-", $candidate->user_phone);
        $opcion = explode(",",$candidate->user_opciones);
        $opciones=array();
        for($i=0;$i<sizeof($opcion);$i++){
            $carrea = new carrera();
            $temp= $carrea->getListNameWhereIn([$opcion[$i]])->get();
            $opciones[$opcion[$i]]=$temp[0]['nombre'];
        }
        $json = $candidate->user_notas;
        $mencion = new menciones_bachillerato();
        $menciones = ManagementList::processListNameCodigo($mencion->getListWhereNotIn([])->get());

        $data = array("candidate" => $candidate,"opciones"=>$opciones, "menciones" => $menciones,'phone' => $phone[1],"json"=> $json);

        Mail::send('layout.emailresumen', $data, function ($message) use($candidate) {
            $message->from("cbbiancocesar@gmail.com", "Resumen de Inscripción");
            $message->to($candidate->user_email, " U.C.V.")->subject("UCV: Resumen de Inscripcion");
        });

    }

}
