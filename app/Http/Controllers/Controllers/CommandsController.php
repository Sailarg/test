<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Artisan;
use Session;
use Redirect;

class CommandsController extends Controller
{

    public function migrate()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        ini_set('max_execution_time', 120);
        Artisan::call('migrate:reset', ["--force"=> true ]);
        Artisan::call('migrate', ["--force"=> true ]);
        Session::flash('message-success', 'Migrated');
        ini_set('max_execution_time', 30);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('/');
    }

    public function seeder()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        ini_set('max_execution_time', 180);
        Artisan::call('db:seed');
        Session::flash('message-success', 'seeder');
        ini_set('max_execution_time', 30);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('/');
    }

    public function all()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        /*
        \DB::statement('DROP DATABASE IF EXISTS ' . $schemaName);
        \DB::statement('CREATE DATABASE ' . $schemaName );
         * */
        ini_set('max_execution_time', 6000);
        Artisan::call('migrate:reset', ["--force"=> true ]);
        Artisan::call('migrate', ["--force"=> true ]);
        Artisan::call('db:seed');
        Session::flash('message-success', 'Migrated and seeder');
        ini_set('max_execution_time', 30);
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        return Redirect::to('/');
    }

}
