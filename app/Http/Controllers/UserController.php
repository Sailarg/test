<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgetRequest;
use App\Models\users;
use Crypt;
use Redirect;
use Hash;
use Session;
use Mail;
use Validator;
use Auth;
use Illuminate\Support\Collection as Collection;

class UserController extends Controller{
    
	public function __construct(){

	}

	public function forget(ForgetRequest $request)
    {
        $user = new users();
        $usuario = $user->getUserByEmail($request["correo"]);

        if(sizeof($usuario) > 0)
        {
            $data = [
                "correo" => $request["correo"],
                "pass" => $user->renewPasswordUser(['ci' => $usuario['user_cedula']])['clave']
                ];

            Mail::send('layout.emailpass', $data, function ($message) use($request) {
                $message->from("cbbiancocesar@gmail.com", "SIGA LAS INSTRUCCIONES PARA ACTIVAR SU CUENTA");
                $message->to($request->input("correo"), "S.N.I. U.C.V.")->subject("SNI: CLAVE DE ACCESO");
            });



            return Redirect::back()->with([
                "confirmar_error" => "alert-danger",
                "message" => "Se le suministro un correo con la informaci&oacute;n pertinente"
            ]);
        }

        return Redirect::back()->with([
            "confirmar_error" => "alert-danger",
            "message" => "El correo suministrado no se encuentra registrado"
        ]);
    }

}