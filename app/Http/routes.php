<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* Quitar estas rutas en ambiente de produccion*/
Route::get('migrate', 'CommandsController@migrate');
Route::get('seed', 'CommandsController@seeder');
Route::get('all', 'CommandsController@all');

Route::get('crear_cuenta', ['uses' => 'HomeController@create', 'as' => 'create_account']);
Route::get('salir', ['uses' => 'PanelController@salir', 'as' => 'out']);
/*GET*/

Route::get('/','HomeController@index');
Route::get('/index','HomeController@getIndex');

Route::group(['prefix' => 'confirm'], function() {

    Route::get('/', ['uses' => 'PanelController@getConfirm', 'as' => 'confirm']);
    Route::get('/{token}', ['uses' => 'PanelController@getConfirm', 'as' => 'confirm']);
    Route::post('/{token}', ['uses' => 'PanelController@postConfirm', 'as' => 'confirm']);
});

Route::group(['middleware' => ['SessionCheck','SessionTimeout']], function()
{
    Route::group(['prefix' => 'aspirante'], function()
    {
        Route::get('inicio', ["uses" => "CandidateController@index", 'as' => 'candidate/index']);
        Route::get('perfil', ["uses" => "CandidateController@profile", 'as' => 'candidate/profile']);
        Route::get('inscripcion', ["uses" => "CandidateController@inscription", 'as' => 'candidate/inscription']);
        Route::get('uploadFile', ["uses" => "CandidateController@getuploadFile", 'as' => 'candidate/uploadFile']);

        Route::post('paso1', ["uses" => "CandidateController@uploadFile", 'as' => 'candidate/uploadFile']);
        Route::post('paso2', ["uses" => "CandidateController@postStep1", 'as' => 'candidate/step_1']);
        Route::post('resumen', ["uses" => "CandidateController@postStep2", 'as' => 'candidate/step_2']);
        Route::post('paso2-2', ["uses" => "CandidateController@postStep1Manual", 'as' => 'candidate/step_1_manual']);
        Route::post('modificar',["uses" => 'CandidateController@edit', 'as' => 'candidate/modify']);
        Route::post('cambiar_clave',["uses" => 'CandidateController@changePassword', 'as' => 'candidate/changePassword']);
    });

    Route::group(['prefix' => 'verificacion'], function()
    {
        Route::get('inicio', ["uses" => "OperatorController@index", 'as' => 'operator/index']);
        Route::get('perfil', ["uses" => "OperatorController@profile", 'as' => 'operator/profile']);
    });

    Route::group(['prefix' => 'validacion'], function()
    {
        Route::get('inicio', ["uses" => "SupervisorController@index", 'as' => 'supervisor/index']);
        Route::get('perfil', ["uses" => "SupervisorController@profile", 'as' => 'supervisor/profile']);
    });

    Route::group(['prefix' => 'Administrador'], function()
    {
        Route::get('inicio', ["uses" => "AdminController@index", 'as' => 'administration/index']);
        Route::get('perfil', ["uses" => "AdminController@profile", 'as' => 'administration/profile']);
    });
});

/*POST*/
Route::post('autentificarte','PanelController@index');
Route::post('registrarse','CandidateController@store');
Route::post('forget','UserController@forget');
