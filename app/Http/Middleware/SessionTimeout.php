<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;
use Illuminate\Session\Store;


class SessionTimeout
{
    protected $session;
    protected $timeout = 300; //five minutes

    public function __construct(Store $session){
        $this->session=$session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$this->session->has('lastActivityTime'))
            $this->session->put('lastActivityTime',time());
        elseif(time() - $this->session->get('lastActivityTime') > $this->timeout){
            $this->session->forget('lastActivityTime');
            Session::flush();
            Auth::logout();
            Session::flash('message-error', 'Session expirada, usted tiene '.$this->timeout/60 .' minutos sin usar el sistema.');
            return Redirect::to('/');
        }
        $this->session->put('lastActivityTime',time());
        return $next($request);
    }
}