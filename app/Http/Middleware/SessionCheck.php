<?php

namespace App\Http\Middleware;

use App\Models\users;
use Closure;
use Auth;
use Session ;
use Redirect;
use Illuminate\Session\Store;

class SessionCheck
{
    protected $session;

    public function __construct(Store $session){
        $this->session = $session;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->session->has("login"))
        {
            return $next($request);
        }
        else
        {
            Session::flush();
            Auth::logout();
            return Redirect::to("/");
        }

    }
}
