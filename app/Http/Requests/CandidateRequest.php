<?php

namespace App\Http\Requests;

class CandidateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'apellido' => 'required',
            'fecha_nacimiento' => 'required',
            'cedula' => 'required|numeric',
            'telefono' => 'required',
            'correo' => 'required|email',
            'confirmar' => 'same:correo',
            'genero' => 'required|min:1',
            'especialidad' => 'required|min:1',
            'estado' => 'required|min:1',
            'municipio' => 'required|min:1',
            'parroquia' => 'required|min:1',
            'liceo' => 'required',
            'liceo_estado' => 'required|min:1',
            'liceo_municipio' => 'required|min:1',
            'liceo_parroquia' => 'required|min:1',
        ];
    }

    public function messages()
    {
        return [
            "confirmar_error" => "alert-danger",
            "message" => "Todos los campos son obligatorios"
        ];
    }
}
