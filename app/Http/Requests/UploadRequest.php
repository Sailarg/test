<?php

namespace App\Http\Requests;

class UploadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file' => 'mimes:pdf',
        ];
    }

    public function messages()
    {
        return [
            "confirmar_error" => "alert-danger",
            "message" => "Todos los campos son obligatorios"
        ];
    }
}
