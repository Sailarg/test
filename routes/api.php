<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['cors'], 'prefix' => 'v1'], function()
{
    Route::post('authenticate', ['as' => 'user.login', 'uses' => 'Auth\LoginController@authenticate']);

    Route::group(['middleware' => ['jwt.verifytoken', 'jwt.auth']], function()
    {
        Route::group(['prefix' => 'user'], function () {
            Route::get('/showAll', ['as' => 'user.showAll', 'uses' => 'UserController@showAll']);
        });

        Route::group(['prefix' => 'task'], function () {
            Route::get('/showAll', ['as' => 'task.showAll', 'uses' => 'TaskController@showAll']);
        });

        Route::group(['prefix' => 'priority'], function () {
            Route::get('/showAll', ['as' => 'priority.showAll', 'uses' => 'PriorityController@showAll']);
        });

        Route::resource('user', 'UserController', ['only' => ['show', 'store', 'update', 'destroy']]);
        Route::resource('task', 'TaskController', ['only' => ['show', 'store', 'update', 'destroy']]);
        Route::resource('priority', 'PriorityController', ['only' => ['show', 'store', 'update', 'destroy']]);
    });
});
