<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class UserTest extends TestCase
{
    use WithoutMiddleware;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->withoutMiddleware();
        $this->message('Iniciando Test Usuario');

        $this->create();
        $this->update();
        $this->destroy();

        $this->message('Test Usuario Finalizado con exito');
        $this->assertTrue(true);
    }

    public function create()
    {
        $attributes =
        [
            'email' => 'usuarioTest@test.com',
            'firstname' => 'usuarioTest',
            'lastname' => 'usuarioTest',
            'password' => '12345678',
        ];

        $this->message('Creando el registro');
        $user = User::create($attributes);
        $user->save();
        $this->imprimir($user);
        $this->message('id: ' . $user->id);
    }

    public function update()
    {
        $attributes =
        [
            'firstname' => 'usuarioTestActualizado'
        ];

        $user = User::where('email', 'usuarioTest@test.com')->first();
        $this->message('Actualizando el registro');
        $user->fill($attributes);
        $user->save();
        $this->imprimir($user);
        $this->assertTrue(true);
    }

    public function destroy()
    {
        $this->message('Eliminando el registro');
        User::where('email', 'usuarioTest@test.com')->delete();
        $this->assertTrue(true);
    }

    public function imprimir($modelo)
    {
        echo 'id: ' . $modelo['id'] . PHP_EOL;
        echo 'nombre: ' . $modelo['firstname'] . PHP_EOL;
        echo 'apellido: ' . $modelo['lastname'] . PHP_EOL;
        echo 'correo: ' . $modelo['email'] . PHP_EOL;
    }

    public function message($message)
    {
        echo $message . PHP_EOL . PHP_EOL;
    }

}
