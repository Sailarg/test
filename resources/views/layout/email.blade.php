<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>SNI | ACTIVAR</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="dist/css/style.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="plugins/iCheck/square/blue.css">
		<!-- JS -->
		<!-- jQuery 2.1.4 -->
		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="dist/js/main.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div colspan="2" style="border: 4px solid #000;border-radius: 10px;font-size: 1.2em;color: white;background-color: #2650A2;">
			<div id="header" style="background-color: #2650A2;text-align: center;margin-bottom:2%">
				<p class="text-center">UNIVERSIDAD CENTRAL DE VENEZUELA</p>
			</div>
			<div id="body" style="background-color:#17467E;height:180px;border-top: 2px solid #FFF;">
				<ol style="margin-top:2%">
					<li>Copie la contrase&ntilde;a que le fue generada</li>
					<li>Active su cuenta, iguiendo el enlace ubicado m&aacute;s abajo.</li>
					<li>Vaya a la pantalla de inicio del sistema</li>
					<li>Pegue la contrase&ntilde;a y usela para acceder por primera vez al sistema.</li>
				</ol>
				<div style="margin-top:2%;margin-left:2%">
					CONTRASE&Ntilde;A: 
					<small class="text-center" style="color:white">{{$pass}}</small></td>
				</div>
				<form action="{{ url('/confirm/' . $code)}}" method="post">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<label class="text-center" style="margin-left:2%">ACTIVE SU CUENTA</label>
					<button type="submit" class="btn btn-default" style="width: 7%;height: 30px;border-radius: 12px;border-top: 2px solid #000;border-left: 2px solid #000;border-right: 2px solid #000;border-bottom: 2px solid #000;">
						Activar
					</button>
				</form>
			</div>
		</div>
	</body>
</html>