<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>SNI | ACTIVAR</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="dist/css/style.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="plugins/iCheck/square/blue.css">
		<!-- JS -->
		<!-- jQuery 2.1.4 -->
		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="dist/js/main.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div colspan="2" style="border: 4px solid #000;border-radius: 10px;text-align: center;font-size: 1.2em;background: #2650A2;color: white;">
			<p class="text-center">UNIVERSIDAD CENTRAL DE VENEZUELA</p>
		</div></br>
		<div style="border: 4px solid #000;border-radius: 10px;margin-top:2%">
			<p style="margin-top:2%;margin-left:2%">Sigas las instrucciones a continuaci&oacute;n</p>
			<div style="margin-top:4%;margin-left:6%">
				<h3>Su contraseña fue restablecida.</h3>
			</div>

			<div style="margin-top:2%;margin-left:2%">
				CONTRASE&Ntilde;A:
				<small class="text-center" style="color:black">{{$pass}}</small></td>
			</div>
		</div>
	</body>
</html>