<!doctype html>
<html class="fix-color">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>{{Lang::get('generals.ucv-title')}}.</title>
		<meta name="keywords" content="UCV SNI SISTEMA" />
		<meta name="description" content="Sistema SNI">
		<meta name="author" content="Equipo de Desarrollo RCG">
		<meta http-equiv=content-type content=text/html; charset=utf-8>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
	<!-- 	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">-->
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{asset('/assets/vendor/bootstrap/css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/font-awesome/css/font-awesome.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/magnific-popup/magnific-popup.css')}}" />
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{asset('assets/vendor/select2/select2.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('/assets/stylesheets/theme.css')}}" />
		<link rel="stylesheet" href="{{asset('/css/style.css')}}" />	
		<link rel="stylesheet" href="{{asset('/assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" />
		<!-- Head Libs -->
		<script src="{{asset('assets/vendor/modernizr/modernizr.js')}}"></script>
	</head>
	<body class="contenedor" ng-app="home" ng-controller="sni-home">
		<header class="config-header" style="max-height: 273px !important;">
			<img src="{{asset('img/header/banner_2.jpg')}}" class="config-image center"/>
			<nav class="navbar config-hr" style="height: 45px !important;">
				<ul class="menu-dashboard navbar navbar-left">
					<li class="@yield('active-home') link-hover"><a href="@yield('link-home')"><i class="fa fa-home"></i> Inicio</a></li>
					<li class="@yield('active-profile') link-hover">
						<a href="{{route('candidate/profile')}}"><i class="fa fa-user"></i> Mi Perfil</a>
					</li>
					<li class="@yield('active-inscripcion') link-hover">
						<a href="{{route('candidate/inscription')}}"><i class="fa fa-check"></i> Inscripci&oacute;n</a>
					</li>
				</ul>
				<ul class="menu-dashboard navbar navbar-right" style="margin-right:2%;">
					@yield('regresar')
					<li class="link-hover"><a href="{{route('out')}}"><i class="fa fa-sign-out"></i> Salir</a></li>
				</ul>
			</nav>
			<div class="config-secundario"></div>
		</header>
		<div class="config-body">
			<div class="row center">
				<div class="col-xs-4 col-md-4" style="background-color: #17467E">
					<div class="bg-primary" style="background-color:#17467E">
						<div class="widget-summary" style="height:75px !important">
							<div class="row ">
								<div class="col-xs-4 col-md-4" >
									<i class="circulito fa fa-user fa-4x" style="margin-top: -15%;margin-left: 25%;"></i>
								</div>
								<div class="col-xs-8 col-md-8">
									<div class="p1">
										<strong class="text-left" style="font-size:1.2em;float:left;margin-top:-2%">

											{{Session::get('nombre')}}</strong></br>
										<small class="text-left" style="font-size:1.2em;float:left;margin-top:-2%">
											C.I. V-{{Session::get('ci')}}</small></br>
										<small class="text-left" style="font-size:1.2em;float:left;margin-top:-2%;margin-bottom:2%">
											@if($role==25)
												Aspirante
											@elseif($role==50)
												Operador
											@elseif($role==75)
												Supervisor
											@elseif($role==100)
												Administrador
											@endif
										</small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="segmento_mensajes" class="col-xs-8 col-md-8 fondo-w" style="width: 66.66666667%;height:75px">
					<label id="message" class=" center alert alert-warning" style="font-size:1.6em">
						<i class="fa fa-exclamation-circle"></i>
						Solo dispone del d&iacute;a de hoy para confirmar su proceso
					</label>
				</div>
			</div>
			<div class="row center" style="width:100%">
				<div class="row fixed-formulario-account" style="width:93%;@yield('fixed-inscripciones')">
					@yield('body')
				</div>
			</div>
			@yield('reloj')
		</div>
		<footer class="config-footer">
			<label class="universidad center">Universidad Central de Venezuela</label>
			<div class="config-principal-footer config-hr-footer">
				<ul class="list-footer">
					<li class="text-center" style="left: 0%;position: relative;">Ciudad Universitaria, Los Chaguaramos / Caracas, Venezuela, C&oacute;digo Postal: 1050</li>
					<li class="text-center" style="left: 0%;position: relative;">Rif: G-20000062-7</li>
				</ul>
			</div>
		</footer>
		@yield("change-password")
		@yield("confirmar")
		@yield("manual")
		@yield("pdf")
		@yield("muestra")
		@yield("error")
		<!-- Vendor -->
		<script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<script src="{{asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{asset('/js/angular/angular.js')}}"></script>
		<script src="{{asset('/js/angular/angular-resource.js')}}"></script>
		<script src="{{asset('/js/app.js')}}"></script>
		<!-- Specific Page Vendor -->
		<script src="{{asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js')}}"></script>
		<script src="{{asset('assets/vendor/select2/select2.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
		<script src="{{asset('assets/vendor/fuelux/js/spinner.js')}}"></script>
		<script src="{{asset('assets/vendor/dropzone/dropzone.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-markdown/js/markdown.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-markdown/js/to-markdown.js')}}"></script>
		<script src="{{asset('assets/vendor/summernote/summernote.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
		<script src="{{asset('assets/vendor/ios7-switch/ios7-switch.js')}}"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('assets/javascripts/theme.js')}}"></script>
		<!-- Theme Initialization Files -->
		<script src="{{asset('assets/javascripts/theme.init.js')}}"></script>
		<!-- Specific Page Vendor -->
		<script src="{{asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
		<script type="text/javascript" src="{{asset('/js/jsonTable.js')}}"></script>
		<!-- Head Libs -->
		<script src="{{asset('/assets/vendor/modernizr/modernizr.js')}}"></script>
		<script src="{{asset('/assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
		<!-- Specific Page Vendor -->
		<script src="{{asset('/assets/vendor/pnotify/pnotify.custom.js')}}"></script>
		<script src="{{asset('/js/modales.js')}}"></script>
	</body>
</html>