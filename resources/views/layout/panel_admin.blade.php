<li class="@yield('active-home')">
    <a href="{{route('bachiller/index')}}">
        <i class="fa fa-home" aria-hidden="true"></i>
        <span>Inicio</span>
    </a>
</li>

<li class="@yield('active-perfil')">
    <a href="{{route('bachiller/profile')}}">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>Mi Perfil</span>
    </a>
</li>

<li class="@yield('active-inscription')">
    <a href="{{route('admin/inscription')}}">
        <i class="fa fa-file-o" aria-hidden="true"></i>
        <span>Inscripcion</span>
    </a>
</li>