<!doctype html>
<html class="fixed">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>U.C.V. | S.N.I.</title>
		<meta name="keywords" content="UCV SNI SISTEMA"/>
		<meta name="description" content="Sistema SNI">
		<meta name="author" content="Equipo de Desarrollo RCG">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{ asset('assets/vendor/font-awesome/css/font-awesome.css')}}" />
		<link rel="stylesheet" href="{{ asset('assets/vendor/magnific-popup/magnific-popup.css')}}" />
		<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
		<link rel="stylesheet" href="{{ asset('assets/vendor/jquery-datatables-bs3/assets/css/datatables.css')}}" />
		<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.css')}}">
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('assets/stylesheets/theme.css')}}" />
		<link rel="stylesheet" href="{{ asset('css/style.css')}}" />
		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('assets/stylesheets/skins/default.css')}}" />
	</head>
	<body>
		<section class="body">
			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<img src="{{ url('assets/images/logo.png')}}" height="35" alt="Porto Admin"/>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
				<!-- start: search & user box -->
				<div class="header-right">
					<span class="separator"></span>
					<div id="userbox" class="userbox">
						<a href="#" data-toggle="dropdown">
							<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
								<span class="name">{{Session::get('name')}}</span>
								<span class="role">
									 @if($role == 100)
                                        Administrador
                                    @elseif($role == 75)
                                        Supervisor
                                    @elseif($role == 50)
                                        Operador
                                    @elseif($role == 25)
                                        Aspirante
                                    @endif
								</span>
							</div>
							<i class="fa custom-caret"></i>
						</a>
						<div class="dropdown-menu">
							<ul class="list-unstyled">
								<li class="divider"></li>
								<li>
									<a role="menuitem" tabindex="-1" href="{{route('candidate/profile')}}"><i class="fa fa-user"></i> Mi Perfil</a>
								</li>
								<li>
									<a role="menuitem" tabindex="-1" href="{{route('out')}}"><i class="fa fa-power-off"></i> Salir</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- end: search & user box -->
			</header>
			<!-- end: header -->
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
					<div class="sidebar-header">
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
                                    @if($role == 100)
                                        @include('layout.panel_admin')
                                    @elseif($role == 75)
                                        @include('layout.panel_supervisor')
                                    @elseif($role == 50)
                                        @include('layout.panel_operador')
                                    @elseif($role == 25)
                                        @include('layout.panel_aspirante')
                                    @endif
								</ul>
							</nav>
						</div>
					</div>
				</aside>

				<!-- end: sidebar -->
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>Bienvenido {{Session::get('name')}}</h2>
						<div class="right-wrapper pull-right" style="margin-right:80px">
							<ol class="breadcrumbs">
								<li>
									<a href="@yield('page-rol')">
										<i class="fa fa-home"></i>
									</a>
								</li>
								<li><span>@yield('page-active')</span></li>
							</ol>
						</div>
					</header>
					<!-- start: page -->
					@yield("body")
					<!-- end: page -->
				</section>
			</div>
		</section>

		<!-- Vendor -->
		<script src="{{ asset('assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{ asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{ asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<script src="{{ asset('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>

		<script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js')}}"></script>
        <script src="{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js')}}"></script>
        
		<!-- Head Libs -->
		<script src="{{ asset('assets/vendor/modernizr/modernizr.js')}}"></script>
		<script src="{{ asset('js/angular/angular.js')}}"></script>
		<script src="{{ asset('js/angular/angular-resource.js')}}"></script>
		<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
		<script src="{{ asset('js/app.js')}}"></script>
		<script src="{{ asset('assets/vendor/modernizr/modernizr.js')}}"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('assets/javascripts/theme.js')}}"></script>
		<!-- Theme Initialization Files -->
		<script src="{{ asset('assets/javascripts/theme.init.js')}}"></script>

	</body>
</html>