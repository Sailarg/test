@section('page-rol')
    {{route('candidate/index')}}
@endsection

<li class="@yield('active-home')">
    <a href="{{route('candidate/index')}}">
        <i class="fa fa-home" aria-hidden="true"></i>
        <span>{{trans('generals.inicio')}}</span>
    </a>
</li>

<li class="@yield('active-perfil')">
    <a href="{{route('candidate/profile')}}">
        <i class="fa fa-user" aria-hidden="true"></i>
        <span>{{trans('generals.my_profile')}}</span>
    </a>
</li>

<li class="@yield('active-inscription')">
    <a href="{{route('candidate/inscription')}}">
        <i class="fa fa-file-o" aria-hidden="true"></i>
        <span>{{trans('generals.inscription')}}</span>
    </a>
</li>