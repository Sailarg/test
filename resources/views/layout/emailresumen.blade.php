<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>SNI | ACTIVAR</title>
		<!-- Tell the browser to be responsive to screen width -->
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<!-- Bootstrap 3.3.5 -->
		<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
		<!-- Font Awesome -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="dist/css/style.css">
		<!-- iCheck -->
		<link rel="stylesheet" href="plugins/iCheck/square/blue.css">
		<!-- JS -->
		<!-- jQuery 2.1.4 -->
		<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
		<script src="dist/js/main.js"></script>
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div colspan="2" style="border: 4px solid #000;border-radius: 10px;text-align: center;font-size: 1.2em;background: #2650A2;color: white;">
			<p class="text-center">UNIVERSIDAD CENTRAL DE VENEZUELA</p>
		</div></br>
		<div style="border: 4px solid #000;border-radius: 10px;margin-top:2%">
			
			<div style="margin-top:2%;margin-left:2%">
				<label>Resumen de inscripci&oacute;n</label>
			</div>
			<h4>Nombre: {{$candidate->user_name}}</h4></br>
			<h4>Apellido: {{$candidate->user_apellido}} </h4></br>
			<h4>Liceo: {{$candidate->user_liceo}}</h4>
			<div style="margin-top:2%;margin-left:2%">
				<label>!!! En Construcci&oacute;n !!!!</label>
			</div>
		</div>
	</body>
</html>