 <?php header('Content-type: application/json; charset=iso-8859-1'); ?>
 <!doctype html>
<html class="fix-color">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>{{Lang::get('generals.ucv-title')}}.</title>
		<meta name="keywords" content="UCV SNI SISTEMA" />
		<meta name="description" content="Sistema SNI">
		<meta name="author" content="Equipo de Desarrollo RCG">
		<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-8">
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Web Fonts  -->
		<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{asset('/assets/vendor/bootstrap/css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/font-awesome/css/font-awesome.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/magnific-popup/magnific-popup.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/bootstrap-datepicker/css/datepicker3.css')}}" />
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('/assets/stylesheets/theme.css')}}" />
		<link rel="stylesheet" href="{{asset('/css/style.css')}}" />
		<!-- Specific Page Vendor CSS -->
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-colorpicker/css/bootstrap-colorpicker.css')}}" />
		<link rel="stylesheet" href="{{asset('assets/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css')}}" />
		<!-- Head Libs -->
		<script src="{{asset('assets/vendor/modernizr/modernizr.js')}}"></script>
	</head>
	<body class="contenedor" ng-app="home" ng-controller="sni-account">
		<header class="config-header">
			<img src="{{asset('img/header/banner.jpg')}}" class="config-image center"/>
			<div class="config-secundario config-hr"></div>
		</header>
		<div class="config-body">
			<div class="row center" style="width:90%">
				<div class="col-xs-12 col-md-12 center" style="margin-bottom: 10px;max-height:56px;height:56px">
					<label id="message" class="text-center alert  @if(!Session::has('confirmar_error')) alert-warning @else {{Session::get('confirmar_error')}} @endif config">
						@if(Session::has("message"))
							<i class="fa fa-times"></i>
							{{Session::get("message")}}
						@else
							@if (count($errors) > 0)
								@foreach ($errors->all() as $error)
									{{ $error }}
								@endforeach
							@else
								<i class="fa fa-exclamation-circle"></i>
								Ingrese sus datos personales
							@endif
						@endif
					</label>
				</div>
			</div>
			<div class="row fixed-formulario-account"  style="width:93%;margin-top:0%;height:906px;font-size:1.2em">
				<?php date_default_timezone_set('America/Caracas');//<--- Hay que mover esto a un sitio seguro.. ?>
				<form id="form-account" action="registrarse" method="post" style="width: 93%;margin-left: 4%;">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<input type="hidden" id="dia_actual" value="{{{ date('D') }}}" />
					<input type="hidden" id="tabla_cedula" value="{{Session::get('configurar_cedula')}}" />
					<input type="hidden" id="mensaje_genero" value="{{Lang::get('generals.mensaje_genero')}}" />
					<input type="hidden" id="mensaje_all" value="{{Lang::get('generals.mensaje_all')}}" />
					<input type="hidden" id="mensaje_estado" value="{{Lang::get('generals.mensaje_estado')}}" />
					<input type="hidden" id="mensaje_nombre" value="{{Lang::get('generals.mensaje_nombre')}}" />
					<input type="hidden" id="mensaje_apellido" value="{{Lang::get('generals.mensaje_apellido')}}" />
					<input type="hidden" id="mensaje_email" value="{{Lang::get('generals.mensaje_email')}}" />
					<input type="hidden" id="mensaje_notmatch" value="{{Lang::get('generals.mensaje_notmatch')}}" />
					<input type="hidden" id="mensaje_liceo" value="{{Lang::get('generals.mensaje_liceo')}}" />
					<input type="hidden" id="mensaje_liceoestado" value="{{Lang::get('generals.mensaje_liceoestado')}}" />
					<div class="row form-group">
						<div class="col-xs-12 col-md-12">
							<label for="message" class="control-label center fixed-title" style="padding-top: 6%;padding-bottom: 2%;">{{Lang::get('generals.datos_personales')}}</label>
						</div>
						<hr/>
						<div class="col-xs-4 col-md-4">
							<label for="cedula" class="control-label fixed-cedula">{{Lang::get('generals.field-ced')}}</label>
							<div class="input-group input-group-icon">
								<input type="text" value="{{old('cedula')}}" placeholder="C&eacute;dula" id="cedula" name="cedula" class="form-control">
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-sort-numeric-asc"></i></span>
								</span>
							</div>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="correo" class="control-label fixed-cedula">{{Lang::get('generals.field_email')}}</label>
							<div class="input-group input-group-icon">
								<input type="email" disabled value="{{old('correo')}}"
								title="Es necesario para poder continuar"
								placeholder="Introduzca su correo." id="correo" name="correo" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-envelope"></i></span>
								</span>
							</div>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="confirmar" class="control-label fixed-cedula">{{Lang::get('generals.field_remenber')}}</label>
							<div class="input-group input-group-icon">
								<input type="email" disabled
								title="Es necesario para poder continuar"
								placeholder="Confirme su correo." id="confirmar" name="confirmar"
								class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-envelope"></i></span>
								</span>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-4 col-md-4">
							<label for="nombre" class="control-label fixed-cedula">{{Lang::get('generals.field_nombre')}}</label>
							<div class="input-group input-group-icon">
								<input disabled type="text" placeholder="Introduzca su nombre."
									   title="Es necesario para poder continuar"
									   id="nombre" name="nombre" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="apellido" class="control-label fixed-cedula">{{Lang::get('generals.field_apellido')}}</label>
							<div class="input-group input-group-icon">
								<input type="text" disabled
								title="Es necesario para poder continuar"
								placeholder="Introduzca su apellido." id="apellido" name="apellido" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-user"></i></span>
								</span>
							</div>
						</div>
						<div class="col-xs-2 col-md-2">
							<label for="genero" class="control-label fixed-cedula">{{Lang::get('generals.field_genero')}}</label>
							<select disabled class="form-control" id="genero" name="genero" required
							title="Es necesario para poder continuar">
								<option value="-1"></option>
								<option value="0">M</option>
								<option value="1">F</option>
							</select>
						</div>
						<div class="col-xs-2 col-md-2">
							<label for="fecha" class="control-label fixed-cedula">Fecha de Nacimiento</label>
							<div  class="input-group input-group-icon">
								<input  disabled type="text" id="fecha_nacimiento" name="fecha_nacimiento"
								title="Es necesario para poder continuar" required data-plugin-datepicker class="form-control" placeholder="dd/mm/yyyy">
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-calendar"></i></span>
								</span>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-2 col-md-2">
							<label for="codigo" class="control-label fixed-cedula">C&oacute;digo</label>
							<select title="Es necesario para poder continuar"
							disabled class="form-control" id="codigo" name="codigo" required>
								<option value="0412" selected>0412</option>
								<option value="0414">0414</option>
								<option value="0416">0416</option>
								<option value="0426">0426</option>
								<option value="0424">0424</option>
							</select>
						</div>
						<div class="col-xs-2 col-md-2">
							<label for="telefono" class="control-label fixed-cedula">Tel&eacute;fono</label>
							<div class="input-group input-group-icon">
								<input type="text" disabled
								title="Es necesario para poder continuar"
								placeholder="#" id="telefono" name="telefono" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-mobile-phone"></i></span>
								</span>
							</div>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="especialidad" class="control-label fixed-cedula">{{Lang::get("generals.field_especialidad")}}</label>
							{!! Form::select('especialidad', $menciones, 0, array('id' => 'especialidad','disabled','required',
							'class' => 'form-control')) !!}
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="liceo" class="control-label fixed-cedula">Nombre de la Instituci&oacute;n</label>
							<div class="input-group input-group-icon">
								<input type="text" disabled
									   title="Es necesario para poder continuar"
									   placeholder="Introduzca el nombre de su instituci&oacute;n." id="liceo" name="liceo" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-institution"></i></span>
								</span>
							</div>
						</div>
					</div>
					<div class="row form-group">
						<div class="col-xs-12 col-md-12" style="margin-top: 6%;margin-bottom: 6%;">
							<label for="message" class="control-label center fixed-title fixed-cedula">Ubicaci&oacute;n geogr&aacute;fica del aspirante</label>
						</div>
						<hr/>
						<div class="col-xs-4 col-md-4">
							<label for="estado" class="control-label fixed-cedula">Indique su estado</label>
							<select disabled
							 class="form-control" id="liceo-estado" name="liceo_estado" required>
								<option value='-1' selected>Seleccione</option>
								<option ng-repeat="data in estados" value='@{{ data.estado }}'>@{{ data.estado }}</option>
							</select>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="municipio" class="control-label fixed-cedula">Municipio</label>
							<select  disabled
							 class="form-control" id="liceo-municipio" name="liceo_municipio" required>
								<option value='-1'>Seleccione</option>
							</select>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="parroquia" class="control-label fixed-cedula">Parroquia</label>
							<select disabled
							title="El campo es necesario para poder continuar"
							 class="form-control" id="liceo-parroquia" name="liceo_parroquia" required>
								<option value='-1'>Seleccione</option>
							</select>
						</div>
						<div class="col-xs-12 col-md-12" style="margin-top: 6%;margin-bottom: 6%;">
							<label for="message" class="control-label center fixed-title">{{Lang::get('generals.ubigeoinstitucion')}}</label>
						</div>
						<hr/>
						<div class="col-xs-4 col-md-4">
							<label for="estado" class="control-label fixed-cedula">Indique su estado</label>
							<select  disabled class="form-control" id="estado" name="estado" required>
								<option value='-1' selected>Seleccione</option>
								<option ng-repeat="data in estados" value='@{{ data.estado }}'>@{{ data.estado }}</option>
							</select>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="municipio" class="control-label fixed-cedula">Municipio</label>
							<select  disabled class="form-control" id="municipio-lista" name="municipio" required>
								<option value='-1'>Seleccione</option>
							</select>
						</div>
						<div class="col-xs-4 col-md-4">
							<label for="parroquia" class="control-label fixed-cedula">Parroquia</label>
							<select disabled class="form-control" id="parroquia-lista" name="parroquia" required>
								<option value='-1'>Seleccione</option>
							</select>
						</div>
					</div>
				</form>
				<div class="row form-group">
					<div class="col-xs-12 col-md-12 text-center" style="margin-top:4%;padding-bottom: 2%;">
						<button id="crear" disabled type="submit" class="btn btn-default btn-lg config-buttom" ng-click="submit()">
							<i class="fa fa-sign-in"></i>
							Crear
						</button>
						<button id="regresar" type="buttom" class="btn btn-default btn-lg config-buttom" ng-click="return()">
							<i class="fa fa-sign-out"></i>
							Regresar
						</button>
					</div>
				</div>
			</div>
			<img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj-account" width="177" height="333"/>
		</div>
		<footer class="config-footer">
			<label class="universidad center">Universidad Central de Venezuela</label>
			<div class="config-principal-footer config-hr-footer">
				<ul class="list-footer">
					<li class="text-center" style="left: 0%;position: relative;">Ciudad Universitaria, Los Chaguaramos / Caracas, Venezuela, C&oacute;digo Postal: 1050</li>
					<li class="text-center" style="left: 0%;position: relative;">Rif: G-20000062-7</li>
				</ul>
			</div>
		</footer>
		<!-- Vendor -->
		<script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<script src="{{asset('assets/vendor/nanoscroller/nanoscroller.js')}}"></script>
		<script src="{{asset('/js/angular/angular.js')}}"></script>
		<script src="{{asset('/js/angular/angular-resource.js')}}"></script>
		<script src="{{asset('/js/app.js')}}"></script>
		<!-- Specific Page Vendor -->
		<script src="{{asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js')}}"></script>
		<script src="{{asset('assets/vendor/select2/select2.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
		<script src="{{asset('assets/vendor/fuelux/js/spinner.js')}}"></script>
		<script src="{{asset('assets/vendor/dropzone/dropzone.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-markdown/js/markdown.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-markdown/js/to-markdown.js')}}"></script>
		<script src="{{asset('assets/vendor/summernote/summernote.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
		<script src="{{asset('assets/vendor/ios7-switch/ios7-switch.js')}}"></script>
		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('assets/javascripts/theme.js')}}"></script>
		<!-- Theme Initialization Files -->
		<script src="{{asset('assets/javascripts/theme.init.js')}}"></script>
	</body>
</html>