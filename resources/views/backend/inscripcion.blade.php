@extends('layout.master')
@section('active-inscripcion')
    link-active
@endsection
@section("link-home")
    {{route('candidate/index')}}
@endsection
@section("regresar")
    <li class="link-hover">
        <a class="link-dashboard" href="{{route('candidate/index')}}">
            <i class="fa fa-chevron-circle-left"></i>
            Regresar
        </a>
    </li>
@endsection
@section("fixed-inscripciones")
    height: 906px;
    max-height: 906px;
    margin-top:  3%;
@endsection
@section("body")
    <div class="row" ng-controller="sni-notas">
        <div class="row center">
            <div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-left: 6%;float: left;">
                <img src="{{asset('/img/pasos/paso1_activo.png')}}" width="300" height="83" style="width:90%;height:40px;">
            </div>
            <div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-left: 6%;">
                <img src="{{asset('/img/pasos/paso2_inactivo1.png')}}" width="300" height="83" style="width:90%;height:40px;">
            </div>
            <div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-right: 6%;float: right">
                <img src="{{asset('/img/pasos/paso3_inactivo.png')}}" width="300" height="83" style="width:90%;height:40px;">
            </div>
        </div>
        <div class="col-xs-12 col-md-12">
            <section id="panel" class="panel">
                <div class="panel-body center fixed-div" style="margin-top: 3%;width: 96%;">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 center" style="width:96%;margin-left:12%">
                            <label class="fixed-labelmodo">Seleccione un tipo de Carga: </label>
                            <button ng-click="pdf()" id="pdf" type="buttom" class="btn btn-default btn-lg config-toggle fixed-buttompdf">
                                <i class="fa fa-file-archive-o"></i>
                                PDF
                            </button>
                            <button ng-click="manual()" id="manual" type="buttom" class="btn btn-default btn-lg config-buttom" style="margin-left: 1%;margin-top: 4%;float: left;width: 26%;">
                                <i class="fa fa-hand-o-up"></i>
                                MANUAL
                            </button>
                        </div>
                        @if($confirmar)
                            <div class="col-xs-5 col-md-5 center" ng-app="home" ng-controller="sni-callmodal" ng-init="call()">
                                <?php $valores = json_decode($jsonpdf); ?>
								<i class="circulito fa fa-user fa-4x" style="margin-top: 0%;margin-left: -3%;color:black;position: relative"></i>
                                <strong class="text-left" style="font-size:1.2em;margin-top:-2%">
                                    {{$valores->nombres}} {{$valores->apellidos}} </strong></br>
                                <small class="text-left" style="font-size:1.2em;margin-top:-2%">
                                    C.I.{{$valores->cedula}} </small></br>
                            </div>
                        @endif
                        <div class="col-xs-12 col-md-12">
                            <div id="forma-pdf" class="mostrar">
                                {!! Form::open(array('route' => 'candidate/uploadFile', 'method' => 'POST', 'files' => true)) !!}
                                <div class="row center form-group" style="width: 62%">
                                    <div id="inputFile" class="col-xs-7 col-md-7 mostrar" style="margin-top:7%">
                                        @if($confirmar)
                                            {!! Form::file('file',array('class' => 'form-control mostrar')) !!}
                                        @else
                                            {!! Form::file('file',array('class' => 'form-control mostrar')) !!}
                                        @endif
                                    </div>
                                    <div class="col-xs-5 col-md-5" style="margin-top:7%">
                                        @if($confirmar)
                                            {!! Form::submit(Lang::get('generals.upload'),array('id' => 'porPDF','class' => 'btn btn-default config-buttom mostrar')) !!}
                                        @else
                                            {!! Form::submit(Lang::get('generals.upload'),array('id' => 'porPDF','class' => 'btn btn-default config-buttom mostrar')) !!}
                                        @endif
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        @if($confirmar)
                            <div id="divpdf" class="col-xs-12 col-md-12 mostrar center"></br>
                                <div class="col-xs-12 col-md-12" style="margin-top:2%;margin-left: 23%;%">
                                    <div class="tabs tabs-default">
                                        <ul class="nav nav-tabs" style="float:left;">
                                            <li class="active">
                                                <a aria-expanded="false" href="#timex1" data-toggle="tab"> Primer A&ntilde;o</a>
                                            </li>
                                            <li class="">
                                                <a aria-expanded="true" href="#timex2" data-toggle="tab"> Segundo A&ntilde;o</a>
                                            </li>
                                            <li class="">
                                                <a aria-expanded="false" href="#timex3" data-toggle="tab"> Tercero A&ntilde;o</a>
                                            </li>
                                            <li class="">
                                                <a aria-expanded="true" href="#timex4" data-toggle="tab"> Cuarto A&ntilde;o</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content" style="width: 60%;">
                                            <div id="timex1" class="tab-pane active">
                                                <table class="table table-striped table-bordered" id="time1" cellspacing="0" width="100%" style="margin-top:10%">
                                                    <thead>
                                                    <tr>
                                                        <th>Materia</th>
                                                        <th style="max-width: 100px;width: 100px;">Notas</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    @foreach($valores->ano1 as $key => $value)
                                                        <tr>
                                                            <td class="text-left">{{$key}}</td>
                                                            <td>{{$value}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="timex2" class="tab-pane">
                                                <table class="table table-striped table-bordered" id="time2" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Materia</th>
                                                        <th style="max-width: 100px;width: 100px;">Notas</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    <?php $valores = json_decode($jsonpdf) ?>
                                                    @foreach($valores->ano2 as $key => $value)
                                                        <tr>
                                                            <td class="text-left">{{$key}}</td>
                                                            <td>{{$value}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="timex3" class="tab-pane">
                                                <table class="table table-striped table-bordered" id="time3" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Materia</th>
                                                        <th style="max-width: 100px;width: 100px;">Notas</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    <?php $valores = json_decode($jsonpdf) ?>
                                                    @foreach($valores->ano3 as $key => $value)
                                                        <tr>
                                                            <td class="text-left">{{$key}}</td>
                                                            <td>{{$value}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div id="timex4" class="tab-pane">
                                                <table class="table table-striped table-bordered" id="time4" cellspacing="0" width="100%">
                                                    <thead>
                                                    <tr>
                                                        <th>Materia</th>
                                                        <th style="max-width: 100px;width: 100px;">Notas</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="tbody">
                                                    <?php $valores = json_decode($jsonpdf) ?>
                                                    @foreach($valores->ano4 as $key => $value)
                                                        <tr>
                                                            <td class="text-left">{{$key}}</td>
                                                            <td>{{$value}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12"></br>
                                        <button id="pasoFinalPdf"  type="button" class="btn btn-default btn-lg config-buttom" data-toggle="modal" data-target="#modalpdf" style="float: left;margin-left: 85%;margin-top: -48.5%;">
                                            <i class="fa fa-arrow-right"></i>
                                            Confirmar
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div id="divmanual" class="col-xs-12 col-md-12 ocultar"></br>
                                <div class="row">
                                    <div class="col-xs-12 col-md-3" style="margin-left:26%">
                                        <label for="state" class="control-label">Seleccione su materia</label>
                                        <select class="form-control" id="materia" name="materia">
                                            <option value="NULL">Seleccione su materia</option>
                                            @foreach($materias as $key => $value)
                                                <option value="{{$value}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-12 col-md-3">
                                        <label for="state" class="control-label">Indique la nota de dicha materia</label>
                                        <input type="number" min="10" max="20" id="notas" name="notas" class="form-control"/>
                                    </div>
                                </div>
                                <div id="scroll" style="overflow:scroll !important;overflow-x:visible !important">
                                    <div class="row" style="width: 63.8%;margin-left: 20%;">
                                        <div class="col-xs-12 col-md-12"></br>
                                            <div class="tabs tabs-default">
                                                <ul class="nav nav-tabs" style="float:left">
                                                    <li class="active">
                                                        <a aria-expanded="false" href="#manual1" data-toggle="tab"> Notas del Primer A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="true" href="#manual2" data-toggle="tab"> Notas del Segundo A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="false" href="#manual3" data-toggle="tab"> Notas del Tercero A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="true" href="#manual4" data-toggle="tab"> Notas del Cuarto A&ntilde;o</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content" style="width: 99.3%;max-height:330px;height:100%">
                                                <div id="manual1" class="tab-pane active">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(1)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias1" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual2" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(2)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias2" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual3" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(3)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias3" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual4" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(4)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias4" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12"></br>
                                    <button id="pasoFinalModal"  type="button" class="btn btn-default btn-lg config-buttom ocultar" data-toggle="modal" data-target="#modalmanual" style="float: left;margin-left: 84%;margin-top: -40%;">
                                        <i class="fa fa-arrow-right"></i>
                                        Confirmar
                                    </button>
                                </div>
                            </div>
                        @else
                            <div id="divmuestra" class="col-xs-12 col-md-12 center mostrar">
                                <div class="row" style="margin-top:4%">
                                    <div class="col-xs-6 col-md-6">
                                        <strong style="font-size:1.5em">Modelo de P.D.F. a cargar en el sistema</strong>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <a href="#" class="thumbnail" data-toggle="modal" data-target="#muestra">
                                            <img src="{{asset('img/muestra.png')}}" alt="..." width="256" height="256">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div id="divmanual" class="col-xs-12 col-md-12 ocultar"></br>
                                <div class="row center" style="position: relative;left: 19.5%;">
									<div class="col-xs-4 col-md-4">
										<div class="clearfix">
											<label for="state" class="control-label" style="float:left">Seleccione su materia</label>
										</div>
                                        <select class="form-control" id="materia" name="materia">
                                            <option value="NULL">Seleccione su materia</option>
                                            @foreach($materias as $key => $value)
                                                <option value="{{$value}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-4 col-md-4">
										<div class="clearfix">
											<label for="state" class="control-label" style="float:left">Indique la nota de dicha materia</label>
										</div>
                                        <input type="number" min="10" max="20" id="notas" name="notas" class="form-control"/>
                                    </div>
                                </div>
                                <div id="scroll" style="overflow:scroll !important;overflow-x:visible !important">
                                    <div class="row" style="width: 63.8%;margin-left: 20%;">
                                        <div class="col-xs-12 col-md-12"></br>
                                            <div class="tabs tabs-default">
                                                <ul class="nav nav-tabs" style="float:left">
                                                    <li class="active">
                                                        <a aria-expanded="false" href="#manual1" data-toggle="tab"> Notas del Primer A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="true" href="#manual2" data-toggle="tab"> Notas del Segundo A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="false" href="#manual3" data-toggle="tab"> Notas del Tercero A&ntilde;o</a>
                                                    </li>
                                                    <li class="">
                                                        <a aria-expanded="true" href="#manual4" data-toggle="tab"> Notas del Cuarto A&ntilde;o</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="tab-content" style="width: 99.3%;max-height:330px;height:100%">
                                                <div id="manual1" class="tab-pane active">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(1)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias1" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual2" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(2)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias2" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual3" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(3)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias3" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                                <div id="manual4" class="tab-pane">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12" style="margin-top: 2%">
                                                            <label style="font-size:1em">Para el registro, se recuerda que debe seleccionar la materia con su nota por a&ntilde;o al finalizar</label>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
                                                            <button data-toggle="toggle" id="add_row" type="buttom" class="btn btn-default config-buttom" ng-click="add(4)">
                                                                <i class="fa fa-plus"></i>
                                                                Agregar
                                                            </button>
                                                        </div>
                                                    </div></br>
                                                    <table class="table table-striped table-bordered" id="materias4" cellspacing="0" width="100%">
                                                        <thead>
                                                        <tr>
                                                            <th>Materia</th>
                                                            <th style="max-width: 100px;width: 100px;">Notas</th>
                                                            <th style="max-width: 100px;width: 100px;">Opciones</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody id="tbody"></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-md-12"></br>
                                    <button id="pasoFinalModal"  type="button" class="btn btn-default btn-lg config-buttom ocultar" data-toggle="modal" data-target="#modalmanual" style="float: left;margin-left: 84%;margin-top: -40%;">
                                        <i class="fa fa-arrow-right"></i>
                                        Confirmar
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section("reloj")
    <img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj-inscription" width="177" height="333" style="margin-top:-359px !important"/>
@endsection
@section("muestra")
    <div class="modal fade" id="muestra" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
        <div class="modal-dialog" role="document">
            <div class="modal-content" style="width: 716px;">
                <header class="modal-header">
                    <div class="row config-principal-modal">
                        <div class="col-xs-10 col-md-10">
                            <h2 class="panel-title fixed-title-modal text-center">Modelo a cargar</h2>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
                        </div>
                    </div>
                    <div class="row config-secundario-modal"></div>
                </header>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 center" style="overflow:scroll;overflow-y:visible;overflow-x:hidden">
                            <img src="{{asset('img/muestra.png')}}" alt="..." width="700" height="716">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-12 center">
                            <button class="btn btn-default btn-lg modal-dismiss config-buttom" aria-hidden="true" data-dismiss="modal">
                                <i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("error")
    <div class="modal fade" id="error" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <div class="row config-principal-modal">
                        <div class="col-xs-10 col-md-10">
                            <h2 class="panel-title fixed-title-modal text-center">No coinciden</h2>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
                        </div>
                    </div>
                    <div class="row config-secundario-modal"></div>
                </header>
                <div class="modal-body fixed-background-modal">
                    <label class="text-center alert alert-warning config-modal" style="text-align:left">
                        <i class="fa fa-exclamation-circle"></i>
                        @if($jsonpdf!="")
                            <?php $valores = json_decode($jsonpdf); ?>
                            @if(Session::get('ci')!=$valores->cedula)
                                <strong>El archivo P.D.F. suministrado no coincide con su perfil, intente nuevamente</strong>
                            @endif
                        @endif
                    </label>
                    <form id="form-step-one" action="{{route('candidate/step_1')}}" method="post">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <input type="hidden" name="notasPDF" id="notasPDF" value="{{$jsonpdf}}"/>
                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 center">
                                <button class="btn btn-default btn-lg modal-dismiss config-buttom" aria-hidden="true" data-dismiss="modal">
                                    <i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("manual")
    <div ng-app="home" ng-controller="sni-finalizar" class="modal fade" id="modalmanual" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <div class="row config-principal-modal">
                        <div class="col-xs-10 col-md-10">
                            <h2 class="panel-title fixed-title-modal text-center">Carga Manual: ¿Est&aacute; usted seguro?</h2>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
                        </div>
                    </div>
                    <div class="row config-secundario-modal"></div>
                </header>
                <div class="modal-body fixed-background-modal">
                    <label class="text-center alert alert-warning config-modal" style="text-align:left">
                        <i class="fa fa-exclamation-circle"></i>
                        {{Lang::get('generals.mensaje_advertencia_paso1')}}
                    </label>
                    <form id="form-step-second-manual" action="{{route('candidate/step_1_manual')}}" method="post">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                        <input type="hidden" name="notasCargadas" id="notasCargadas"/>
                        <div class="row form-group">
                            <div class="col-xs-12 col-md-12 center">
                                <button
                                        ng-click="paso3()"
                                        id="confirmar"  type="button" class="btn btn-default btn-lg config-buttom" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-arrow-right"></i>
                                    Confirmar
                                </button>
                                <button class="btn btn-default btn-lg modal-dismiss config-buttom" aria-hidden="true" data-dismiss="modal">
                                    <i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("pdf")
    <div ng-app="home" ng-controller="sni-finalizar" class="modal fade" id="modalpdf" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <header class="modal-header">
                    <div class="row config-principal-modal">
                        <div class="col-xs-10 col-md-10">
                            <h2 class="panel-title fixed-title-modal text-center">Carga por pdf: ¿Est&aacute; usted seguro?</h2>
                        </div>
                        <div class="col-xs-2 col-md-2">
                            <i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
                        </div>
                    </div>
                    <div class="row config-secundario-modal"></div>
                </header>
                <div class="modal-body fixed-background-modal">
                    <label class="text-center alert alert-warning config-modal" style="text-align:left">
                        <i class="fa fa-exclamation-circle"></i>
                        {{Lang::get('generals.mensaje_advertencia_paso2')}}
                    </label>
                    @if($jsonpdf!="")
                        <form id="form-step-second" action="{{route('candidate/step_1')}}" method="post">
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                            <input type="hidden" name="notasPDF" id="notasPDF" value="{{$jsonpdf}}"/>
                            <div class="row form-group">
                                <div class="col-xs-12 col-md-12 center">
                                    <button
                                            id="confirmar"  type="submit" class="btn btn-default btn-lg config-buttom" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-arrow-right"></i>
                                        Confirmar
                                    </button>
                                    <button class="btn btn-default btn-lg modal-dismiss config-buttom" aria-hidden="true" data-dismiss="modal">
                                        <i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection