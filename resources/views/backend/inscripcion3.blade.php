@extends('layout.master')
@section('active-inscripcion')
    link-active
@endsection
@section("link-home")
    {{route('candidate/index')}}
@endsection
@section("regresar")
    <li class="link-hover">
        <a class="link-dashboard" href="{{route('candidate/index')}}">
            <i class="fa fa-chevron-circle-left"></i>
            Regresar
        </a>
    </li>
@endsection
@section("fixed-inscripciones")
    height: 906px;
    max-height: 906px;
    margin-top:  3%;
@endsection
@section("body")
    <div class="row" ng-controller="sni-notas">
        <div class="col-xs-12 col-md-12">
            <section id="panel" class="panel">
				<div class="row">
					<?php $valores = json_decode($candidate); ?>
					<div class="col-xs-6 col-md-6" style="position: relative;left: 2%">
						<div class="row">
							<div class="col-xs-12 col-md-12" style="margin-top:4%;">
								<strong style="font-size:2em">Datos personales en el sistema</strong>
							</div>
							<div class="col-xs-2 col-md-2">
								<i class="circulito fa fa-user fa-4x" style="float:left;margin-top:2%;color:black"></i>
							</div>
							<div class="col-xs-10 col-md-10">
								<ul class="datosPersonales">
									<li><i class="fa fa-sort-numeric-asc"></i>&nbsp;C.I.V.-{{$valores->user_cedula}}</li>
									<li><i class="fa fa-user"></i>&nbsp;Aspirante. {{$valores->user_name}} {{$valores->user_apellido}}</li>
									<li><i class="fa fa-envelope-o">&nbsp;{{$valores->user_email}}</i></li>
									<li><i class="fa fa-mobile fa-2x"></i>&nbsp;{{$valores->user_phone}}</li>
									<li><i class="fa 
									@if($valores->user_genero==0)
										fa-male
									@else
										fa-female
									@endif"></i>&nbsp;{{$valores->user_genero==0?"Masculino":"Femenino"}}</li>
									<li><i class="fa fa-calendar"></i>&nbsp;{{$valores->user_fecha_nacimiento}}</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xs-6 col-md-6" style="position: relative;left: 2%">
						<div class="row">
							<div class="col-xs-12 col-md-12" style="margin-top:4%;">
								<strong style="font-size:2em">Datos de ubicaci&oacute;n en el sistema</strong>
							</div>
							<div class="col-xs-2 col-md-2">
								<i class="circulito fa fa-globe fa-4x" style="float:left;margin-top:2%;color:black"></i>
							</div>
							<div class="col-xs-10 col-md-10">
								<ul class="datosPersonales">
									<li>
										<i class="fa fa-home fa fa-2x"></i>
										<ul class="subLista">
											<li style="font-size: 1.5em;">Ubicaci&oacute;n del aspirante</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_estado}}</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_municipio}}</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_parroquia}}</li>
										</ul>
									</li>
									<li style="margin-top: 10%;margin-left: -6%;">
										<i class="fa fa-graduation-cap fa fa-2x" style="left: 38%;position: relative;"></i>
										<ul class="subLista">
											<li style="font-size: 1.5em;">Ubicaci&oacute;n de la instituci&oacute;n</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_estado}}</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_municipio}}</li>
											<li><i class="fa fa-circle"></i>&nbsp;{{$valores->user_parroquia}}</li>
										</ul>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
                <div class="panel-body center fixed-div" style="margin-top: 3%;width: 96%;height: 581px;">
                    <div class="row" style="overflow:scroll !important;overflow-x:visible !important;overflow-y:visible !important">
						<?php $valores = json_decode($candidate); ?>
						<div class="col-xs-4 col-md-4">
							<strong style="font-size:2em;float:left;margin-bottom:2%">Carreras seleccionadas</strong>
							<table class="table table-striped table-bordered" id="seleccion1" cellspacing="0" width="100%" style="margin-top:2%">
								<thead>
								<tr>
									<th>C&oacute;digo</th>
									<th style="max-width: 200px;width: 200px;">Carrera</th>
								</tr>
								</thead>
								<tbody id="tbody">
									@foreach($opciones as $key => $value)
										<tr>
											<td class="text-left">{{$key}}</td>
											<td>{{$value}}</td>
										</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="col-xs-8 col-md-8">
							@if($json!="")
								<?php $valores = json_decode($json); ?>
								<strong style="font-size:2em;float:left;margin-bottom:2%">Notas cargadas</strong></br>
								<div class="tabs tabs-default" style="margin-top:2%">
									<ul class="nav nav-tabs" style="float:left;">
										<li class="active">
											<a aria-expanded="false" href="#timex1" data-toggle="tab"> Primer A&ntilde;o</a>
										</li>
										<li class="">
											<a aria-expanded="true" href="#timex2" data-toggle="tab"> Segundo A&ntilde;o</a>
										</li>
										<li class="">
										<a aria-expanded="false" href="#timex3" data-toggle="tab"> Tercero A&ntilde;o</a>
										</li>
										<li class="">
											<a aria-expanded="true" href="#timex4" data-toggle="tab"> Cuarto A&ntilde;o</a>
										</li>
									</ul>
									<div class="tab-content">
										<div id="timex1" class="tab-pane active">
											<?php $valores = json_decode($json); ?>
											@if(sizeof($valores->ano1)==0)
												<div class="row">
													<div class="col-xs-12 col-md-12">
														<strong>No se encontraron notas cargadas para este a&ntilde;o</strong>
													</div>
												</div>
											@else
												<table class="table table-striped table-bordered" id="seleccion1" cellspacing="0" width="100%" style="margin-top:10%">
													<thead>
														<tr>
															<th>Materia</th>
															<th style="max-width: 100px;width: 100px;">Notas</th>
														</tr>
													</thead>
													<tbody id="tbody">
														@foreach($valores->ano1 as $key => $value)
															<tr>
																<td class="text-left">{{$key}}</td>
																<td>{{$value}}</td>
															</tr>
														@endforeach
													</tbody>
												</table>
											@endif
										</div>
										<div id="timex2" class="tab-pane">
											<?php $valores = json_decode($json); ?>
											@if(sizeof($valores->ano2)==0)
												<div class="row">
													<div class="col-xs-12 col-md-12">
														<strong>No se encontraron notas cargadas para este a&ntilde;o</strong>
													</div>
												</div>
											@else
												<table class="table table-striped table-bordered" id="seleccion2" cellspacing="0" width="100%" style="margin-top:10%">
													<thead>
														<tr>
															<th>Materia</th>
															<th style="max-width: 100px;width: 100px;">Notas</th>
														</tr>
													</thead>
													<tbody id="tbody">
														@foreach($valores->ano2 as $key => $value)
														<tr>
															<td class="text-left">{{$key}}</td>
															<td>{{$value}}</td>
														</tr>
														@endforeach
													</tbody>
												</table>
											@endif
										</div>
										<div id="timex3" class="tab-pane">
											<?php $valores = json_decode($json); ?>
											@if(sizeof($valores->ano3)==0)
												<div class="row">
													<div class="col-xs-12 col-md-12">
														<strong>No se encontraron notas cargadas para este a&ntilde;o</strong>
													</div>
												</div>
											@else
												<table class="table table-striped table-bordered" id="seleccion3" cellspacing="0" width="100%" style="margin-top:10%">
													<thead>
														<tr>
															<th>Materia</th>
															<th style="max-width: 100px;width: 100px;">Notas</th>
														</tr>
													</thead>
													<tbody id="tbody">
														@foreach($valores->ano3 as $key => $value)
															<tr>
																<td class="text-left">{{$key}}</td>
																<td>{{$value}}</td>
															</tr>
														@endforeach
													</tbody>
												</table>
											@endif
										</div>
										<div id="timex4" class="tab-pane">
											<?php $valores = json_decode($json); ?>
											@if(sizeof($valores->ano4)==0)
												<div class="row">
													<div class="col-xs-12 col-md-12">
														<strong>No se encontraron notas cargadas para este a&ntilde;o</strong>
													</div>
												</div>
											@else
												<table class="table table-striped table-bordered" id="seleccion4" cellspacing="0" width="100%" style="margin-top:10%">
													<thead>
														<tr>
															<th>Materia</th>
															<th style="max-width: 100px;width: 100px;">Notas</th>
														</tr>
													</thead>
													<tbody id="tbody">	
														@foreach($valores->ano4 as $key => $value)
														<tr>
															<td class="text-left">{{$key}}</td>
															<td>{{$value}}</td>
														</tr>
														@endforeach
													</tbody>
												</table>
											@endif
										</div>
									</div>
								</div>
							@endif
						</div>
					</div>
                </div>
            </section>
        </div>
    </div>
@endsection
@section("reloj")
    <img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj-inscription" width="177" height="333" style="margin-top:-359px !important"/>
@endsection