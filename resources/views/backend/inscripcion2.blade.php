@extends('layout.master')
@section('active-inscripcion')
	link-active
@endsection
@section("link-home")
	{{route('candidate/index')}}
@endsection
@section("regresar")
	<li class="link-hover">
		<a class="link-dashboard" href="{{route('candidate/index')}}">
			<i class="fa fa-chevron-circle-left"></i>
			Regresar
		</a>
	</li>
@endsection
@section("fixed-inscripciones")
	height: 906px;
	max-height: 906px;
	margin-top: 3%;
@endsection
@section("body")
	<div class="row" ng-app="home" ng-controller="sni-inscripciones">
		<div class="row center">
			<div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-left: 6%;float: left;">
				<img src="{{asset('/img/pasos/paso1_inactivo.png')}}" width="300" height="83" style="width:90%;height:40px;">
			</div>
			<div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-left: 6%;">
				<img src="{{asset('/img/pasos/paso2_activo.png')}}" width="300" height="83" style="width:90%;height:40px;">
			</div>
			<div class="col-xs-3 col-md-3 center" style="margin-top: 6%;margin-right: 6%;float: right">
				<img src="{{asset('/img/pasos/paso3_inactivo.png')}}" width="300" height="83" style="width:90%;height:40px;">
			</div>
		</div>
		<div class="col-xs-12 col-md-12">
			<section class="panel">
				<div class="panel-body center" style="font-size: 1.5em;margin-top: 6%;width:99%">
					<div class="row">
						<div class="col-xs-12 col-md-4" style="margin-top:1%;position: relative;left: 2%;margin-bottom:1%">
							<?php $valores = json_decode($areas) ?>
							<input type="hidden" name="datos" id="datos" value="{{$areas}}">
							<label for="state" class="control-label" style="float:left">Indique su &aacute;rea</label>
							<select class="form-control" id="area" name="area">
								<option value="NULL">Indique su area</option>
								@foreach($valores as $a)
									<option value="{{$a->nombre}}">{{$a->nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-12 col-md-4" style="margin-top:1%;position: relative;left: 2%;margin-bottom:2%">
							<label for="state" class="control-label" style="float:left" >Indique la facultad</label>
							<select class="form-control" id="facultad-lista" name="facultad-lista" disabled>
								<option value="NULL">Indique la facultad</option>
							</select>
						</div>
						<div class="col-xs-12 col-md-4" style="margin-top:1%;position: relative;left: 2%;">
							<label for="state" class="control-label" style="float:left">Indique la carrera</label>
							<select class="form-control" id="carrera-lista" name="carrera-lista" disabled>
								<option value="NULL">Indique la carrera</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-12 center" style="margin-top: 2%">
							<button id="add_row" disabled type="buttom" class="btn btn-default config-buttom" ng-click="add()">
								<i class="fa fa-plus"></i>
								Agregar
							</button>
							<button id="add_reset" type="buttom" class="btn btn-default config-buttom" ng-click="reset()">
								<i class="fa fa-undo"></i>
								Limpiar
							</button>
						</div>
					</div>
					<div class="row center">
						<div class="col-xs-12 col-md-12">
							<table class="table table-striped table-bordered fixed-table" id="seleccion" cellspacing="0" width="100%" style="margin-top:4%">
								<thead>
								<tr>
									<th>Opci&oacute;n</th>
									<th>&Aacute;rea</th>
									<th>Facultad</th>
									<th>Carrera</th>
									<th>Opciones</th>
								</tr>
								</thead>
								<tbody id="tbody"></tbody>
							</table>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-xs-12 col-md-12 fixed-buttom">
			<button disabled id="confirmar"  type="button" class="btn btn-default btn-lg config-buttom" data-toggle="modal" data-target="#myModal" style="float: left;margin-left: 44%;margin-top: -7%;">
				<i class="fa fa-arrow-right"></i>
				Confirmar
			</button>
		</div>
	</div>
@endsection
@section("reloj")
	<img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj-inscription" width="177" height="333"/>
@endsection
@section("confirmar")
	<div ng-app="home" ng-controller="sni-confirmar" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<header class="modal-header">
					<div class="row config-principal-modal">
						<div class="col-xs-10 col-md-10">
							<h2 class="panel-title fixed-title-modal text-center">¿Est&aacute; usted seguro?</h2>
						</div>
						<div class="col-xs-2 col-md-2">
							<i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
						</div>
					</div>
					<div class="row config-secundario-modal"></div>
				</header>
				<div class="modal-body fixed-background-modal">
					<label class="text-center alert alert-warning config-modal" style="text-align:left">
						<i class="fa fa-exclamation-circle"></i>
						{{Lang::get('generals.mensaje_advertencia_paso2')}}
					</label>
					<form id="form-step-one" action="{{route('candidate/step_2')}}" method="post">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<input type="hidden" name="seleccionar_area" id="json"/>
						<div class="row form-group">
							<div class="col-xs-12 col-md-12 center">
								<button
										ng-click="paso2()"
										id="confirmar"  type="button" class="btn btn-default btn-lg config-buttom" data-toggle="modal" data-target="#myModal">
									<i class="fa fa-arrow-right"></i>
									Confirmar
								</button>
								<button class="btn btn-default btn-lg modal-dismiss config-buttom" aria-hidden="true" data-dismiss="modal">
									<i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
@endsection