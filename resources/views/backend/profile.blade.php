@extends("layout.master")
@section("title")
	Mi Perfil
@endsection
@section("active-perfil")
	nav-active
@endsection
@section("page-active")
	Mi Perfil
@endsection
@section("body")
	<div class="row">	
		<div class="col-xs-12 col-md-12">
			<section class="panel">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12 col-md-12">
							<label class="control-label"><i class="fa fa-lg fa-book"></i> Datos Personales</label>
							<hr/>
						</div>
						<div class="col-xs-12 col-md-12">
							<form id="form-perfil" method="post" action="modify">
								<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
								@if(Session::has('activo_user'))
									@foreach(Session::get('activo_user') as $user)
										<input type="hidden" name="ced" value="{{ $user->user_cedula }}" />
										<input type="hidden" name="gen" value="{{ $user->user_genero }}" />
										<input type="hidden" name="esp" value="{{ $user->user_especialidad }}" />
										<div class="row form-group">
											<div class="col-xs-12 col-md-4">
												<label for="name" class="control-label">Nombres</label>
												<div class="input-group input-group-icon">
													<input type="text" placeholder="{{$user->user_name}}" 
													title="Debe ingresar un nombre valido para el registro"
													id="name" name="name" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-user"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="apellido" class="control-label">Apellidos</label>
												<div class="input-group input-group-icon">
													<input type="text" 
													title="Debe ingresar un apellido valido para el registro"
													placeholder="{{$user->user_apellido}}" id="apellido" name="apellido" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-user"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="cedula" class="control-label">C&eacute;dula de Identidad</label>
												<div class="input-group input-group-icon">
													<input type="text"
													placeholder="{{$user->user_cedula}}"
													disabled id="cedula" name="cedula" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-sort-numeric-asc"></i></span>
													</span>
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12 col-md-4">
												<label for="phone" class="control-label">N&uacute;mero de Contacto</label>
												<div class="input-group input-group-icon">
													<input type="text" 
													title="Debe ingresar un n&uacute;mero valido para el registro"
													placeholder="Introduzca su numero de Contacto" id="phone" name="phone" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-mobile-phone"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="correo" class="control-label">Correo Electr&oacute;nico</label>
												<div class="input-group input-group-icon">
													<input type="email" 
													disabled
													title="Debe ingresar un correo valido para el registro"
													placeholder="{{$user->user_email}}" id="correo" name="correo" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-envelope"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="liceo" class="control-label">Instituci&oacute;n</label>
												<div class="input-group input-group-icon">
													<input type="text" 
													title="Debe ingresar una instuci&oacute;n para el registro"
													placeholder="{{$user->user_liceo}}" id="liceo" name="liceo" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-institution"></i></span>
													</span>
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12 col-md-2">
												<label for="genero" class="control-label">Genero</label>
												<select class="form-control" id="genero" name="genero">	
													<option value="-1">Indique su genero</option>
													@if($user->user_genero=="0")
														<option value="0" selected>Masculino</option>
														<option value="1">Femenino</option>
													@else
														<option value="0">Masculino</option>
														<option value="1" selected>Femenino</option>
													@endif
												</select>
											</div>
											<div class="col-xs-12 col-md-2">
												<label for="especialidad" class="control-label">Especialidad</label>
												<select class="form-control" id="especialidad" name="especialidad">	
													<option value="NULL">Indique su especialidad</option>
													@if($user->user_especialidad=="C")
														<option value="C" selected>Ciencias</option>
														<option value="H">Humanidades</option>
													@else
														<option value="C">Ciencias</option>
														<option value="H" selected>Humanidades</option>
													@endif
												</select>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="correo" class="control-label">Contrase&ntilde;a</label>
												<div class="input-group input-group-icon">
													<input type="password" 
													title="Debe ingresar una contrase&ntilde;a valida"
													placeholder="Introduzca un contrase&ntilde;a valida." id="pwd" name="pwd" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-envelope"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="confirmar" class="control-label">Confirme Contrase&ntilde;a</label>
												<div class="input-group input-group-icon">
													<input type="password" 
													title="Deben coincidir las contrase&ntilde;a"
													placeholder="Introduzca un contrase&ntilde;a valida." id="confirmar" name="confirmar" 
													class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-envelope"></i></span>
													</span>
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12 col-md-12">
												<label class="control-label">Ubicaci&oacute;n Geografica</label>
												<hr/>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-xs-12 col-md-4">
												<label for="state" class="control-label">Indique su Estado</label>
												<label for="municipio" class="control-label">Estado</label>
												<div id="mun" class="input-group input-group-icon mostrar">
													<input disabled
													type="text" placeholder="{{$user->user_estado}}" id="estado" name="estado" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-thumb-tack"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="municipio" class="control-label">Municipio</label>
												<div id="mun" class="input-group input-group-icon mostrar">
													<input disabled
													type="text" placeholder="{{$user->user_municipio}}" id="municipio" name="municipio" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-thumb-tack"></i></span>
													</span>
												</div>
											</div>
											<div class="col-xs-12 col-md-4">
												<label for="parroquia" class="control-label">Parroquia</label>
												<div class="input-group input-group-icon">
													<input 
													disabled
													title="Debe ingresar una Parroquia valida"
													type="text" placeholder="{{$user->user_parroquia}}" id="parroquia" name="parroquia" class="form-control">
													<span class="input-group-addon">
														<span class="icon"><i class="fa fa-thumb-tack"></i></span>
													</span>
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-md-12 text-center">
												<button type="submit" class="btn btn-default"><i class="fa fa-edit"></i> Modificar</button>
											</div>
										</div>
									@endforeach
								@endif
							</form>
						</div>
						<div class="col-xs-12 col-md-12" style="margin-top:10px">
							@if(Session::has("confirmar_error") && Session::get("confirmar_error")==1)
								@if(Session::has("message"))
									<label class="text-center alert alert-danger config"><i class="fa fa-times"></i> {{Session::get("message")}}<label>
								@endif
							@elseif(Session::has("confirmar_error") && Session::get("confirmar_error")==0)
								@if(Session::has("message"))
									<label class="text-center alert alert-success config"><i class="fa fa-check"></i> {{Session::get("message")}}<label>
								@endif
							@elseif(Session::has("confirmar_error") && Session::get("confirmar_error")==2)
								@if(Session::has("message"))
									<label class="text-center alert alert-warning config"><i class="fa fa-warning"></i> {{Session::get("message")}}<label>
								@endif
							@else
								@if (count($errors) > 0)<!--SI EL JS SE DESACTIVA IMPRIMIR $error POR UNA TAG DE HTML-->
									@foreach ($errors->all() as $error)
										<label class="text-center alert alert-danger config"><i class="fa fa-times"></i> {{ $error }} <label>
									@endforeach
								@endif
							@endif
						</div>
					</div>
				</div>
			</section>	
		</div>
	</div>
@endsection