 @extends('layout.master')
 @section('active-profile')
 	link-active
 @endsection
 @section('active-hover-profile')
 	link-hover
 @endsection
 @section("link-home")
 	{{route('candidate/index')}}
@endsection
 @section("regresar")
 	<li class="link-hover">
 		<a class="link-dashboard" href="{{route('candidate/index')}}">
 			<i class="fa fa-chevron-circle-left"></i>
			Regresar
		</a>
	</li>
@endsection
 @section("fixed-inscripciones")
	 height: 906px;
	 max-height: 906px;
	 margin-top: 3%;
 @endsection
 @section('body')
	 <?php date_default_timezone_set('America/Caracas');//<--- Hay que mover esto a un sitio seguro.. ?>
	 <form id="form-account" action="registrarse" method="post" class="config-form" ng-app="home" ng-controller="sni-profile">
		 <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		 <input type="hidden" id="dia_actual" value="{{{ date('D') }}}" />
		 <input type="hidden" id="tabla_cedula" value="{{Session::get('configurar_cedula')}}" />
		 <input type="hidden" id="mensaje_genero" value="{{Lang::get('generals.mensaje_genero')}}" />
		 <input type="hidden" id="mensaje_all" value="{{Lang::get('generals.mensaje_all')}}" />
		 <input type="hidden" id="mensaje_estado" value="{{Lang::get('generals.mensaje_estado')}}" />
		 <input type="hidden" id="mensaje_nombre" value="{{Lang::get('generals.mensaje_nombre')}}" />
		 <input type="hidden" id="mensaje_apellido" value="{{Lang::get('generals.mensaje_apellido')}}" />
		 <input type="hidden" id="mensaje_email" value="{{Lang::get('generals.mensaje_email')}}" />
		 <input type="hidden" id="mensaje_notmatch" value="{{Lang::get('generals.mensaje_notmatch')}}" />
		 <input type="hidden" id="mensaje_liceo" value="{{Lang::get('generals.mensaje_liceo')}}" />
		 <input type="hidden" id="mensaje_liceoestado" value="{{Lang::get('generals.mensaje_liceoestado')}}" />
		 <div class="row form-group">
			 <div class="col-xs-12 col-md-12">
				 <label for="message" class="control-label center fixed-title" style="padding-top: 6%;padding-bottom: 2%;">{{Lang::get('generals.datos_personales')}}</label>
			 </div>
			 <hr/>
			 <div class="col-xs-6 col-md-6">
				 <label for="cedula" class="control-label fixed-cedula">{{Lang::get('generals.field-ced')}}</label>
				 <div class="input-group input-group-icon">
					 <input disabled type="text" value="{{$candidate->user_cedula}}" placeholder="{{$candidate->user_cedula}}" id="cedula" name="cedula" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-sort-numeric-asc"></i></span>
							</span>
				 </div>
			 </div>
			 <div class="col-xs-6 col-md-6">
				 <label for="correo" class="control-label fixed-cedula">{{Lang::get('generals.field_email')}}</label>
				 <div class="input-group input-group-icon">
					 <input type="email" disabled value="{{$candidate->user_email}}" placeholder="{{$candidate->user_email}}"
							title="Es necesario para poder continuar"
							placeholder="Introduzca su correo." id="correo" name="correo" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-envelope"></i></span>
							</span>
				 </div>
			 </div>
		 </div>
		 <div class="row form-group">
			 <div class="col-xs-6 col-md-6">
				 <label for="nombre" class="control-label fixed-cedula">{{Lang::get('generals.field_nombre')}}</label>
				 <div class="input-group input-group-icon">
					 <input  type="text" placeholder="Introduzca su nombre."
							value="{{$candidate->user_name}}"  placeholder="{{$candidate->user_name}}"
							id="nombre" name="nombre" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-user"></i></span>
							</span>
				 </div>
			 </div>
			 <div class="col-xs-6 col-md-6">
				 <label for="apellido" class="control-label fixed-cedula">{{Lang::get('generals.field_apellido')}}</label>
				 <div class="input-group input-group-icon">
					 <input type="text"
							value="{{$candidate->user_apellido}}"  placeholder="{{$candidate->user_apellido}}"
							placeholder="Introduzca su apellido." id="apellido" name="apellido" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-user"></i></span>
							</span>
				 </div>
			 </div>
		</div>
		<div class="row form-group">
			 <div class="col-xs-3 col-md-1">
				 <label for="genero" class="control-label fixed-cedula">{{Lang::get('generals.field_genero')}}</label>
				 <select  class="form-control" id="genero" name="genero">
					 <option value="-1"></option>
					 @if($candidate->user_genero=="0")
					 	<option value="0" selected>M</option>
					 	<option value="1">F</option>
					 @else
						 <option value="0">M</option>
						 <option value="1" selected>F</option>
					 @endif
				 </select>
			 </div>
			 <div class="col-xs-3 col-md-3">
				 <label for="fecha" class="control-label fixed-cedula">Fecha de Nacimiento</label>
				 <div  class="input-group input-group-icon">
					 <input  type="text" id="fecha_nacimiento" name="fecha_nacimiento"
							value="{{$candidate->user_fecha_nacimiento}}"  placeholder="{{$candidate->user_fecha_nacimiento}}"
							title="Es necesario para poder continuar"  data-plugin-datepicker class="form-control" placeholder="dd/mm/yyyy">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-calendar"></i></span>
							</span>
				 </div>
			 </div>
			 <div class="col-xs-3 col-md-4">
				 <label for="codigo" class="control-label fixed-cedula">C&oacute;digo</label>
				 <select title="Es necesario para poder continuar"
						 class="form-control" id="codigo" name="codigo">
					 <option value="0412" selected>0412</option>
					 <option value="0414">0414</option>
					 <option value="0416">0416</option>
					 <option value="0426">0426</option>
					 <option value="0424">0424</option>
				 </select>
			 </div>
			 <div class="col-xs-3 col-md-4">
				 <label for="telefono" class="control-label fixed-cedula">Tel&eacute;fono</label>
				 <div class="input-group input-group-icon">
					 <input type="text"
							value="{{$phone}}"  placeholder="{{$phone}}"
							placeholder="#" id="telefono" name="telefono" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-mobile-phone"></i></span>
							</span>
				 </div>
			 </div>
		 </div>
		 <div class="row form-group">
			 <div class="col-xs-6 col-md-6">
				 <label for="especialidad" class="control-label fixed-cedula">{{Lang::get("generals.field_especialidad")}}</label>
				 {!! Form::select('especialidad', $menciones, 0, array('id' => 'especialidad',
				 'class' => 'form-control')) !!}
			 </div>
			 <div class="col-xs-6 col-md-6">
				 <label for="liceo" class="control-label fixed-cedula">Nombre de la Instituci&oacute;n</label>
				 <div class="input-group input-group-icon">
					 <input type="text"
							value="{{$candidate->user_liceo}}"  placeholder="{{$candidate->user_liceo}}"
							placeholder="Introduzca el nombre de su instituci&oacute;n." id="liceo" name="liceo" class="form-control">
							<span class="input-group-addon">
								<span class="icon"><i class="fa fa-institution"></i></span>
							</span>
				 </div>
			 </div>
		 </div>
		 <div class="row form-group">
			 <div class="col-xs-12 col-md-12" style="margin-top: 2%;margin-bottom: 2%;">
				 <label for="message" class="control-label center fixed-title fixed-cedula" style="padding-bottom: 2%;">Ubicaci&oacute;n geogr&aacute;fica del aspirante</label>
			 </div>
			 <hr/>
			 <div class="col-xs-4 col-md-4">
				 <label for="estado" class="control-label fixed-cedula">Indique su estado</label>
				 <select class="form-control" id="liceo-estado" name="liceo_estado" style="margin-bottom: 10%;">
					 <option value='-1' selected>Seleccione</option>
					 <option ng-repeat="data in estados track by $index" value='@{{ data.estado }}'>@{{ data }}</option>
				 </select>
			 </div>
			 <div class="col-xs-4 col-md-4">
				 {{{ header('Content-Type: application/json; charset=UTF-8') }}}
				 <label for="municipio" class="control-label fixed-cedula">Municipio</label>
				 <select
						  class="form-control" id="liceo-municipio" name="liceo_municipio" style="margin-bottom: 10%;">
					 <option value='-1'>Seleccione</option>
				 </select>
			 </div>
			 <div class="col-xs-4 col-md-4">
				 <label for="parroquia" class="control-label fixed-cedula">Parroquia</label>
				 <select
						 title="El campo es necesario para poder continuar"
						 class="form-control" id="liceo-parroquia" name="liceo_parroquia" style="margin-bottom: 10%;">
					 <option value='-1'>Seleccione</option>
				 </select>
			 </div>
			 <div class="col-xs-12 col-md-12" style="margin-top: 2%;margin-bottom: 2%;" style="padding-bottom: 2%;">
				 <label for="message" class="control-label center fixed-title">{{Lang::get('generals.ubigeoinstitucion')}}</label>
			 </div>
			 <hr/>
			 <div class="col-xs-4 col-md-4">
				 <label for="estado" class="control-label fixed-cedula">Indique su estado</label>
				 <select   class="form-control" id="estado" name="estado" >
					 <option value='-1' selected>Seleccione</option>
					 <option ng-repeat="data in estados" value='@{{ data.estado }}'>@{{ data.estado }}</option>
				 </select>
			 </div>
			 <div class="col-xs-4 col-md-4">
				 <label for="municipio" class="control-label fixed-cedula">Municipio</label>
				 <select   class="form-control" id="municipio-lista" name="municipio" >
					 <option value='-1'>Seleccione</option>
				 </select>
			 </div>
			 <div class="col-xs-4 col-md-4">
				 <label for="parroquia" class="control-label fixed-cedula">Parroquia</label>
				 <select  class="form-control" id="parroquia-lista" name="parroquia" >
					 <option value='-1'>Seleccione</option>
				 </select>
			 </div>
		 </div>
	 </form>
	 <div class="row form-group" style="margin-top: 4%;">
		 <div class="col-xs-12 col-md-12 text-center" style="margin-top:2%;padding-bottom: 2%;">
			 <button id="crear"  type="submit" class="btn btn-default config-buttom" ng-click="submit()">
				 <i class="fa fa-sign-in"></i>
				 Modificar
			 </button>
			 <button id="regresar" type="buttom" class="btn btn-default config-buttom" ng-click="return()">
				 <i class="fa fa-sign-out"></i>
				 Regresar
			 </button>
			 <button id="change" type="buttom" class="btn btn-default config-buttom" data-toggle="modal" data-target="#myModal">
				 <i class="fa fa-lock"></i>
				 Cambiar contrase&ntilde;a
			 </button>
		 </div>
	 </div>
 @endsection
 @section('reloj')
	 <img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj-perfil" width="177" height="333"/>
 @endsection
 @section('change-password')
	 <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
		 <div class="modal-dialog" role="document">
			 <div class="modal-content">
				 <form id="form-change" action="{{route('candidate/changePassword')}}" method="post">
					 <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					 <input type="hidden" name="old_pwd" value="{{$candidate->user_password}}" />
					 <header class="modal-header">
						 <div class="row config-principal-modal">
							 <div class="col-xs-10 col-md-10">
								 <h2 class="panel-title fixed-title-modal text-center">¿{{Lang::get('generals.forget-title')}}?</h2>
							 </div>
							 <div class="col-xs-2 col-md-2">
								 <i class="fa fa-times-circle-o fa-2x fixed-close modal-dismiss" aria-hidden="true" data-dismiss="modal"></i>
							 </div>
						 </div>
						 <div class="row config-secundario-modal"></div>
					 </header>
					 <div class="modal-body">
						<div class="row form-group">
						 <div class="col-xs-12 col-md-12">
							 <label for="correo" class="control-label">Contrase&ntilde;a Actual</label>
							 <div class="input-group input-group-icon">
								 <input type="password"
										title="Debe ingresar un correo valido para el registro"
										placeholder="Introduzca un correo valido" id="actual" name="actual" class="form-control" required>
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-envelope"></i></span>
									</span>
							 </div>
						 </div>
						 <div class="col-xs-12 col-md-12">
							 <label for="confirmar" class="control-label">Nueva Contrase&ntilde;a</label>
							 <div class="input-group input-group-icon">
								 <input type="password"
										title="Deben coincidir los correos para el registro"
										placeholder="Introduzca un correo valido" id="pwd" name="clave"
										class="form-control" required>
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-envelope"></i></span>
									</span>
							 </div>
						 </div>
						 <div class="col-xs-12 col-md-12">
							 <label for="confirmar" class="control-label">Confirmar Contrase&ntilde;a</label>
							 <div class="input-group input-group-icon">
								 <input type="password"
										title="Deben coincidir los correos para el registro"
										placeholder="Introduzca un correo valido" id="confirmar" name="confirmar"
										class="form-control" required>
									<span class="input-group-addon">
										<span class="icon"><i class="fa fa-envelope"></i></span>
									</span>
							 </div>
						 </div>
						</div>
						<div class="row form-group">
							 <div class="col-xs-12 col-md-12 text-center">
								 <button type="submit" class="btn btn-default config-buttom" ng-click="enviar()"><i class="fa fa-edit"></i> Modificar</button>
								 <button class="btn btn-default modal-dismiss config-buttom" type="button" data-dismiss="modal">
									 <i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}
								 </button>
							 </div>
						</div>
					 </div>
				 </form>
			 </div>
		 </div>
	 </div>
 @endsection
