<!doctype html>
<html class="fix-color">
	<head>
		<!-- Basic -->
		<meta charset="UTF-8">
		<title>{{Lang::get('generals.ucv-title')}}.</title>
		<meta name="keywords" content="UCV" />
		<meta name="description" content="Sistema SNI">
		<meta name="author" content="Equipo de Desarrollo RCG">
		<meta http-equiv=content-type content=text/html; charset=utf-8>
		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{asset('/assets/vendor/bootstrap/css/bootstrap.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/font-awesome/css/font-awesome.css')}}" />
		<link rel="stylesheet" href="{{asset('/assets/vendor/magnific-popup/magnific-popup.css')}}" />
		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{asset('/assets/stylesheets/theme.css')}}" />
		<link rel="stylesheet" href="{{asset('/css/style.css')}}" />
	</head>
	<body class="contenedor" ng-app="home" ng-controller="sni-home">
		<header class="config-header">
			<img src="{{asset('img/header/banner.jpg')}}" class="config-image center"/>
			<div class="config-secundario config-hr"></div>
		</header>
		<div class="config-body">
			<a href="http://ucv.ve/">
				<img src="{{ asset('img/logos/ucvlogo.jpg') }}" class="config-ucv" height="128" width="128"/>
			</a>
			<a href="http://www.ucv.ve/estructura/secretaria-general/browse/1.html">
				<img src="{{ asset('img/logos/secretarialogo.png') }}" class="config-secretaria" height="128" width="110"/>
			</a>
			<label id="message" class="text-center alert {{Session::get('confirmar_error')}} config">
				@if(Session::has("message"))
					<i class="fa fa-check"></i> {{Session::get('message')}}
				@else
					<i class="fa fa-exclamation-circle"></i> {{Lang::get('generals.mensaje_advertencia_inicial')}}
				@endif
			</label>
			<div class="row fixed-formulario">
				<form id="form-login" action="autentificarte" method="post">
					<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
					<input type="hidden" id="mensaje" value="{{Lang::get('generals.mensaje_advertencia_inicial')}}" />
					<div class="form-group">
						<div class="col-xs-12 col-md-12 margin">
							<label for="ced" class="control-label fixed-cedula">{{Lang::get('generals.field-ced')}}</label>
							<div class="input-group input-group-icon">
								<input type="number"
									   placeholder="Introduzca su c&eacute;dula de aspirante" id="ced" name="ced" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-user"></i></span>
								</span>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 col-md-12 margin">
								<label for="ced" class="control-label fixed-password">{{Lang::get('generals.field-pwd')}}</label>
								<div class="input-group input-group-icon">
									<input type="password"
										   title="El campo es necesario"
										   placeholder="Introduzca su clave de acceso" id="pwd" name="pwd" class="form-control" required>
								<span class="input-group-addon">
									<span class="icon"><i class="fa fa-lock"></i></span>
								</span>
								</div>
							</div>
						</div>
						<div class="row form-group fixed">
							<div class="col-xs-6 col-md-6 col-fixed-6">
								<label for="account" class="control-label">
									<a href="#account" class="text-center config-link account" data-toggle="modal" data-target="#account">
										{{Lang::get('generals.create_account')}}
									</a>
								</label>
							</div>
							<div class="col-xs-6 col-md-6 col-fixed-6">
								<label for="forget" class="control-label">
									<a href="#forget" class="config-link forget" data-toggle="modal" data-target="#forget-pwd">
										{{Lang::get('generals.forget_account')}}
									</a>
								</label>
							</div>
						</div>
						<div class="row form-group fixed">
							<div class="col-xs-12 col-md-12" style="left: 10%;position: relative;margin-left: -39px;">
								<div class="g-recaptcha center" data-sitekey="6LcePAATAAAAAGPRWgx90814DTjgt5sXnNbV5WaW"></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-xs-12 col-md-12 margin">
								<button type="submit" class="btn btn-default btn-lg config-buttom center">
									<i class="fa fa-sign-in"></i>
									{{Lang::get('generals.button-entry')}}
								</button>
							</div>
						</div>
					</div>
				</form>
				<div class="g-recaptcha" data-sitekey="6Lf-jSETAAAAAG3Sj9bBFjy3tclkPUB8-J2gH_gR"></div>
			</div>
			<img src="{{ asset('img/transparencias/relojtransparencia.png') }}" class="config-reloj" width="177" height="333"/>
		</div>
		<footer class="config-footer">
			<label class="universidad center">Universidad Central de Venezuela</label>
			<div class="config-principal-footer config-hr-footer">
				<ul class="list-footer">
					<li class="text-center" style="left: 0%;position: relative;">Ciudad Universitaria, Los Chaguaramos / Caracas, Venezuela, C&oacute;digo Postal: 1050</li>
					<li class="text-center" style="left: 0%;position: relative;">Rif: G-20000062-7</li>
				</ul>
			</div>
		</footer>
		<div class="modal fade" id="forget-pwd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="form-forget" action="forget" method="post">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<header class="modal-header">
							<div class="row config-principal-modal">
								<div class="col-xs-10 col-md-10">
									<h2 class="fixed-title-modal text-center">¿{{Lang::get('generals.forget-title')}}?</h2>
								</div>
								<div class="col-xs-2 col-md-2">
									<i class="fa fa-times-circle-o fa-2x fixed-close" data-dismiss="modal" aria-hidden="true"></i>
								</div>
							</div>
						</header>
						<div class="modal-body">
							<div class="row form-group">
								<div class="col-xs-12 col-md-6">
									<label for="correo" class="control-label">{{Lang::get('generals.field_email')}}</label>
									<div class="input-group input-group-icon">
										<input type="email"
											   title="Debe ingresar un correo valido para el registro"
											   placeholder="Introduzca un correo valido" id="correo" name="correo" class="form-control" required>
										<span class="input-group-addon">
											<span class="icon"><i class="fa fa-envelope"></i></span>
										</span>
									</div>
								</div>
								<div class="col-xs-12 col-md-6">
									<label for="confirmar" class="control-label">{{Lang::get('generals.field_remenber')}}</label>
									<div class="input-group input-group-icon">
										<input type="email"
											   title="Deben coincidir los correos para el registro"
											   placeholder="Introduzca un correo valido" id="confirmar" name="confirmar"
											   class="form-control" required>
										<span class="input-group-addon">
											<span class="icon"><i class="fa fa-envelope"></i></span>
										</span>
									</div>
								</div>
							</div>
							<div class="row form-group">
								<div class="row form-group">
									<div class="col-xs-12 col-md-12 text-center">
										<button type="buttom" class="btn btn-default config-buttom"><i class="fa fa-edit"></i> {{Lang::get('generals.button-recordar')}}</button>
										<button class="btn btn-default config-buttom" data-dismiss="modal"><i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" id="account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="opacity: 1;" ng-app="home" ng-controller="sni-change-password">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<form id="form-forget" action="forget" method="post">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<header class="modal-header">
							<div class="row config-principal-modal">
								<div class="col-xs-10 col-md-10">
									<h2 class="panel-title fixed-title-modal text-center">{{Lang::get('generals.button-forget')}}!!</h2>
								</div>
								<div class="col-xs-2 col-md-2">
									<i class="fa fa-times-circle-o fa-2x fixed-close" data-dismiss="modal" aria-hidden="true"></i>
								</div>
							</div>
						</header>
						<div class="modal-body fixed-background-modal">							
							<label class="text-center alert alert-warning config-modal">
								<i class="fa fa-exclamation-circle"></i>
								{{Lang::get('generals.mensaje_advertencia_inicial')}}
							</label>
							<div class="row form-group">
								<div class="col-xs-12 col-md-12 text-center">
									<a href="{{route('create_account')}}" class="btn btn-default config-buttom"><i class="fa fa-edit"></i> {{Lang::get('generals.button-continue')}}</a>
									<button class="btn btn-default config-buttom" data-dismiss="modal"><i class="fa fa-sign-out"></i> {{Lang::get('generals.button-remove')}}</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<script src='https://www.google.com/recaptcha/api.js?hl=es'></script>
		<!-- Vendor -->
		<script src="{{asset('assets/vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
		<script src="{{asset('assets/vendor/magnific-popup/magnific-popup.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
		<!-- Head Libs -->
		<script src="{{asset('assets/vendor/modernizr/modernizr.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery-validation/jquery.validate.js')}}"></script>
		<script src="{{asset('js/angular/angular.js')}}"></script>
		<script src="{{asset('js/angular/angular-resource.js')}}"></script>
		<script src="{{asset('js/app.js')}}"></script>
		<!-- Specific Page Vendor -->
		<script src="{{asset('assets/vendor/pnotify/pnotify.custom.js')}}"></script>
		<script src="{{asset('js/modales.js')}}"></script>
	</body>
</html>