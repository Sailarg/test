<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_name');
            $table->string('user_apellido');
            $table->integer('user_cedula')->unique();
            $table->string('user_phone');
            $table->string('user_fecha_nacimiento');
            $table->enum('user_genero',['0','1']);
            $table->integer('user_especialidad');
            $table->string('user_status');
            $table->string('user_email')->unique();
            $table->string('user_password',255);
            $table->string('user_estado');
            $table->string('user_municipio');
            $table->string('user_parroquia');
            $table->string('user_liceo');
            $table->string('user_liceo_estado');
            $table->string('user_liceo_municipio');
            $table->string('user_liceo_parroquia');
            $table->string('user_pdf');
            $table->text('user_notas');
            $table->string('user_promedio');
            $table->string('user_promedio2');
            $table->string('user_promedio3');
            $table->string('user_opciones');
            $table->string('api_token',255);
            $table->enum('api_token_estatus',['0','1']);
            $table->enum('user_paso',['1','2','3','4']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user');
    }
}
