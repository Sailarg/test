<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrespondenciaTabla extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('correspondencia', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('correspondencia_dia',['MON','TUE','WED','THU','FRI']);
            $table->enum('correspondencia_ced',['0','1','2','3','4','5','6','7','8','9']);
            $table->integer('user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('correspondencia');
    }
}
