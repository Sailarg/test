<?php

use App\Models\estados;
use App\Models\municipios;
use App\Models\parroquias;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class query_ubicacion_geografica_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = new estados();
        $municipio = new municipios();
        $parroquias = new parroquias();

        $lista_estados = $estados->getListWhereNotIn([])->get();
        $faker = Faker::create();

        for($i = 0; $i < sizeof($lista_estados); $i++)
        {
            $municipios = $municipio->getListWhereEstadoIdIn([$lista_estados[$i]->id])->get();
            for($j = 0; $j < sizeof($municipios); $j++)
            {
                $municipios[$j]["parroquia"] = $parroquias->getListWhereMunicipioIdIn([$municipios[$j]->id])->get();
            }

            $lista_estados[$i]["municipio"] = $municipios;
        }

        $json = json_encode($lista_estados);

        \DB::table('ubicacion_geografica')->insert(
            array(
                'id' => 1,
                'lista' => $json,
                'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
            )
        );
    }
}
