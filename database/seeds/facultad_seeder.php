<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class facultad_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $nombre = array
        (
            'Lista',
            'Arquitectura', 'Ciencias', 'Ingeniería',
            'Farmacia', 'Odontologia', 'Medicina',
            'Ciencias Económicas y Sociales', 'Humanidades y Educación','Ciencias Juridicas y Politicas',
            'Agronomía', 'Ciencias Veterinaria'
        );

        $areas = array
        (
            0,
            1, 1, 1,
            2, 2, 2,
            3, 3, 3,
            4, 4
        );

        for($i = 1; $i < sizeof($nombre); $i++)
        {
            \DB::table('facultad')->insert(
                array(
                    'id' => $i,
                    'area_id' => $areas[$i],
                    'nombre' => $nombre[$i],
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                    'updated_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
        }

    }
}
