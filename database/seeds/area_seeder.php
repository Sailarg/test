<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class area_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $areas = array
        (
            'Lista',
            'Ciencia y Tecnología',
            'Ciencias de la Salud',
            'Ciencias Sociales',
            'Ciencias del Agro y del Mar'
        );

        for($i = 1; $i <sizeof($areas); $i++)
        {
            \DB::table('area')->insert(
                array(
                    'id' => $i,
                    'nombre' => $areas[$i],
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                    'updated_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
        }

    }
}
