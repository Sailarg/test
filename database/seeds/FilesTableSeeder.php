<?php

use Illuminate\Database\Seeder;
use App\Models\File;
use Faker\Factory as Faker;

/**
 * Description of FilesTableSeeder
 *
 * @author Gary Romero
 */

class FilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index)
        {
            $file = File::create([
                'name' =>  'avatar.jpg',
            ]);
            $file ->save();
        }
    }
}
