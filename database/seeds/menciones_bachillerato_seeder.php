<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class menciones_bachillerato_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $carerras = array
        (
            'Lista',
            'ACERVO PATRIMONIAL', 'ADMINISTRACIÓN FINANCIERA', 'ADMINISTRACIÓN COMERCIAL', 'ADMINISTRACIÓN DE PERSONAL', 'ADMINISTRACIÓN DE SERVICIOS',
            'ADMINISTRACIÓN DE SERVICIOS DE SALUD', 'ADMINISTRACIÓN Y PROCESAMIENTO DE DATOS', 'ADUANA', 'AGROTURISMO', 'ARTE PURO',
            'ARTE DE FUEGO', 'ARTES CULINARIAS', 'ARTES ESCÉNICAS', 'ARTES MUSICALES', 'ARTES VISUALES Y DEL ESPACIO',
            'ARTES GRÁFICAS', 'ASISTENCIA GERENCIAL', 'CIENCIAS', 'CIENCIAS LABORATORIO CLÍNICO', 'CIENCIAS PRODUCCIÓN PECUARIA',
            'CIENCIAS BÁSICAS Y TECNOLÓGICAS', 'CIENCIAS SOCIALES Y TECNOLÓGICAS', 'CIENCIAS AGRÍCOLAS', 'CIUDADANÍA AMBIENTAL', 'CONSTRUCCIÓN CIVIL',
            'CONTABILIDAD', 'DIBUJO TÉCNICO', 'DISEÑO TEATRAL', 'ELECTRICIDAD', 'ELECTRÓNICA',
            'HIDROCARBUROS', 'HUMANIDADES', 'HUMANIDADES PARA EL DESARROLLO ENDÓGENO', 'INFORMÁTICA',
            'INSPECCIÓN SANITARIA', 'LABORATORIO CLÍNICO', 'MANTENIMIENTO INTEGRAL DE EDIFICACIONES', 'EDIFICACIONES',
            'MECÁNICA', 'MECÁNICA AUTOMOTRIZ', 'MECÁNICA DE MANTENIMIENTO', 'MERCADEO', 'MUSICA',
            'MÁQUINAS Y HERRAMIENTAS', 'PETROQUÍMICA', 'PROCESAMIENTO DE DATOS', 'PRODUCCIÓN AGRÍCOLA', 'PROMOCIÓN Y DESARROLLO COMUNITARIO',
            'PROMOCIÓN Y GESTIÓN AMBIENTAL', 'PUERICULTURA', 'QUÍMICA INDUSTRIAL', 'REFRIGERACIÓN Y AIRE ACONDICIONADO', 'REGISTRO Y ESTADÍSTICAS DE SALUD',
            'SEGUROS', 'TECNOLOGÍA  PESQUERA', 'TECNOLOGÍA DE ALIMENTOS', 'TELECOMUNICACIONES', 'TURISMO',
            'TÉNCIAS DE ALIMENTOS','PRODUCCIÓN PECUARIA'
        );

        for($i = 1; $i < sizeof($carerras); $i++)
        {
            \DB::table('menciones_bachillerato')->insert(
                array(
                    'id' => $i,
                    'codigo' => 2000 + $i,
                    'nombre' => $carerras[$i],
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
        }

    }
}
