<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class correspondencia_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'MON',
            'correspondencia_ced' => '1',
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'TUE',
            'correspondencia_ced' => '2'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'WED',
            'correspondencia_ced' => '3'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'WED',
            'correspondencia_ced' => '4'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'THU',
            'correspondencia_ced' => '5'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'THU',
            'correspondencia_ced' => '6'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'THU',
            'correspondencia_ced' => '7'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'FRI',
            'correspondencia_ced' => '8'
        ]);

        \DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'FRI',
            'correspondencia_ced' => '9'
        ]);
		
		\DB::table('correspondencia')->insert([
            'correspondencia_dia' => 'FRI',
            'correspondencia_ced' => '0'
        ]);
    }
}
