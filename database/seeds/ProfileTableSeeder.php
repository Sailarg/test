<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profile;
use App\Models\File;

/**
 * Description of ProfileTableSeeder
 *
 * @author Gary Romero
 */

class ProfileTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::get();

        foreach ($users as $user)
        {
            $perfil = Profile::where('user_id', $user->id)->first();
            if(is_null($perfil) || empty($perfil))
            {
                DB::table('profiles')->insert([
                    'user_id' => $user->id,
                    'image_id' => File::select(['id'])->orderByRaw('RAND()')->first()->id,
                    'created_at' => DB::raw('CURRENT_TIMESTAMP'),
                    'updated_at' => DB::raw('CURRENT_TIMESTAMP')
                ]);
            }
        }
    }
}
