<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class materia_liceo_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker = Faker::create();
        $codigos = array
        (
            0,
            1001, 1002, 1003, 1004,
            1005, 1006, 1007, 1008,
            1009, 1010, 1011, 1012,
            1013
        );

        $carerras = array
        (
            'Lista',
            'MATEMÁTICA', 'CIENCIAS BIOLÓGICAS', 'QUÍMICA', 'FÍSICA',
            'CASTELLANO Y LITERATURA', 'EDUCACIÓN ARTÍSTICA', 'HISTORIA DE VENEZUELA', 'HISTORIA UNIVERSAL',
            'HISTORIA CONTEMPORÁNEA DE VENEZUELA', 'FILOSOFÍA', 'INGLES', 'HISTORIA DE VENEZUELA (CÁTEDRA BOLIVARIANA)',
            'DIBUJO'
        );

        for($i = 1; $i < sizeof($codigos); $i++)
        {
            \DB::table('materia_liceo')->insert(
                array(
                    'id' => $i,
                    'codigo' => $codigos[$i],
                    'nombre' => $carerras[$i],
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
        }

    }
}
