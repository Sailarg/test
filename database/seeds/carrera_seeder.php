<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class carrera_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        /*
         * Facultad id
         * Arquitectura 1
         * Ciencias 2
         * Ingenieria 3
         * Farmacia 4
         * Odontologia 5
         * Medicina 6
         * FaCES 7
         * Humanidades y Educación 8
         * Ciencias Juridicas y Politicas 9
         * Agronomía 10
         * Ciencias Veterinaria 11
         */
        $faker = Faker::create();
        $id = 1;

        /*
         * Se agrega en la base de datos las carreras asociadas al area Ciencia y Tecnología
         * cada posicion del arreglo correponde con su codigo, nombre y area
         * */

        $codigos = array
        (
            0,
            10000, 10001, 10002, 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010,
            10011, 10012, 10013, 10014, 10015, 10016, 10017, 10232, 10233, 10234, 10235,
            10236, 10237, 10238, 10239, 10240, 10371, 12199, 13076, 14221
        );

        $carerras = array
        (
            'Lista',
            'Química', 'Física', 'Matemática', 'Biología', 'Ingeniería Geofísica', 'Ingeniería Metalúrgica', 'Ingeniería de Minas', 'Ingeniería Geológica', 'Ingeniería Geodésica', 'Ingeniería Hidrometeorológica',
            'Ingeniería de Petróleo', 'Computación', 'Ingeniería Eléctrica', 'Ingeniería Mecánica', 'Ingeniería Química', 'Geoquímica', 'Ingeniería Civil', 'Arquitectura', 'Ingeniería Geofísica (Cagua)', 'Ingeniería Metalúrgica (Cagua)',
            'Ingeniería de Minas (Cagua)', 'Ingeniería Geológica (Cagua)', 'Ingeniería Geodésica (Cagua)', 'Ingeniería de Petróleo (Cagua)', 'Ingeniería Eléctrica (Cagua)', 'Ingeniería Química (Cagua)',
            'Ingeniería Civil (Cagua)', 'Arquitectura (EUS) ( Barquisimeto)', 'Ingeniería Mecánica (Cagua)', 'Ingeniería Hidrometeorológica (Cagua)', 'Ingeniería de Procesos Industriales (Cagua)'
        );

        $areas_id = array
        (
            0,
            2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3,
            2, 3, 3, 3, 2, 3, 1, 3, 3, 3, 3,
            3, 3, 3, 3, 3, 1, 3, 3, 3
        );

        $importates = array
        (
            "Agronomía" => "1001,1005,1004",
            "Arquitectura" => "1001,1005,1006,1013",
            "Ciencias" => "1001,1005",
            "Ciencias Económicas y Sociales" => "1001,1005,1007,1008,1012,1009",
            "Ciencias Juridicas y Politicas" =>"1001,1005,1007,1008,1012,1009",
            "Ciencias Veterinaria" => "1005,1002,1003",
            "Farmacia" => "1001,1002,1003",
            "Humanidades y Educación" => "1005,1010",
            "Ingeniería" => "1001,1005,1004",
            "Medicina" => "1005,1002,1003",
            "Odontologia" => "1005,1002,1003",
        );

        $importa = "";
        $mencion = "";
        $especialidades = array
        (
            'Agronomía' => '2018,2056,2023,2047,2059,2056,2009',
            'Arquitectura' => '2018,2032,2037,2038,2025,2028',
            'Ciencias' => '2018,2034,2021,2056,2057,2049',
            'Ciencias Económicas y Sociales' => '2032,2002,2017,2029,2042,2054,2058,2022,2001,2024,2004,2046,2002,2003,2029,2042,2048,2049,2017,2007,2005,2008,2058',
            'Ciencias Juridicas y Politicas' => '2032',
            'Ciencias Veterinaria' => '2018,2061,2009,2055',
            'Farmacia' => '2018,2036,2006,2053,2035',
            'Humanidades y Educación' => '2058,2012,2013,2001,2014,2015,2024,2011,2010,2004,2033,2049,2048,2058,2016,2043,2010,2014,2028',
            'Ingeniería' => '2025,2029,2030,2052,2044,2041,2025,2030,2039,2041,2051,2040,2052,2057,2031,2045',
            'Medicina' => '2018,2050,2036,2006,2053,2035,2050',
            'Odontologia' => '2018,2036,2006,2053,2035',
        );

        for($i = 1; $i < sizeof($codigos); $i++)
        {
            switch($areas_id[$i])
            {
                case 1:
                    $importa = $importates["Arquitectura"];
                    $mencion = $especialidades["Arquitectura"];
                break;

                case 2:
                    $importa = $importates["Ciencias"];
                    $mencion = $especialidades["Ciencias"];
                break;

                case 3:
                    $importa = $importates["Ingeniería"];
                    $mencion = $especialidades["Ingeniería"];
                break;
            }

            \DB::table('carrera')->insert(
                array(
                    'id' => $id,
                    'codigo' => $codigos[$i],
                    'nombre' => $carerras[$i],
                    'facultad_id' => $areas_id[$i],
                    'materias_importantes' => $importa,
                    'menciones' => $mencion,
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
            $id++;
        }

        /*
         * Se agrega en la base de datos las carreras asociadas al area Ciencias de la Salud
         * */

        $codigos = array
        (
            0,
            10018, 10019, 10020, 10021, 10022, 10023,
            10024, 10025, 10026, 10027, 10028, 10029,
            10030, 12219, 12266, 13775, 14301
        );

        $carerras = array
        (
            'Lista',
            'Medicina', 'Enfermería', 'Bioanálisis', 'Nutrición y Dietética', 'Odontología', 'Farmacia',
            'Fisioterapia (Técnica)', 'Terapia Ocupacional (Técnica)', 'Radiología e Imagenología', 'Citotecnología (Técnica)', 'Tecnología Cardiopulmonar', 'Inspección Sanitaria',
            'Información de  la Salud', 'Fisioterapia', 'Enfermeria (Técnica)', 'Terapia Ocupacional', 'Inspección de Salud Pública'
        );

        $areas_id = array
        (
            0,
            6, 6, 6, 6, 5, 4,
            6, 6, 6, 6, 6, 6,
            6, 6, 6, 6, 6
        );

        for($i = 1; $i < sizeof($codigos); $i++)
        {
            switch($areas_id[$i])
            {
                case 4:
                    $importa = $importates["Farmacia"];
                    $mencion = $especialidades["Farmacia"];
                    break;

                case 5:
                    $importa = $importates["Odontologia"];
                    $mencion = $especialidades["Odontologia"];
                    break;

                case 6:
                    $importa = $importates["Medicina"];
                    $mencion = $especialidades["Medicina"];
                    break;
            }

            \DB::table('carrera')->insert(
                array(
                    'id' => $id,
                    'codigo' => $codigos[$i],
                    'nombre' => $carerras[$i],
                    'facultad_id' => $areas_id[$i],
                    'materias_importantes' => $importa,
                    'menciones' => $mencion,
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
            $id++;
        }

        /*
         * Se agrega en la base de datos las carreras asociadas al area Ciencias Sociales
         * */

        $codigos = array
        (
            0,
            10031, 10032, 10033, 10034, 10035, 10036,
            10037, 10038, 10039, 10040, 10041, 10042,
            10043, 10044, 10045, 10046, 10047, 10048,
            10049, 10050, 10051, 10648, 10372, 10851,
            10765, 10766
        );

        $carerras = array
        (
            'Lista',
            'Educación', 'Economía', 'Ciencias Estadísticas', 'Ciencias Actuariales', 'Contaduría Pública', 'Administración',
            'Comunicación Social', 'Trabajo Social', 'Psicología', 'Geografía', 'Sociología', 'Antropología',
            'Derecho', 'Estudios Internacionales', 'Estudios Políticos y Administrativos', 'Filosofía', 'Letras', 'Historia',
            'Bibliotecología y Archivología', 'Idiomas Modernos', 'Artes', 'Educación (EUS) (Barcelona)', 'Educación (EUS) (Barquisimeto)', 'Educación (EUS) (Caracas)',
            'Educación (EUS) (Ciudad Bolívar)', 'Educación (EUS) (Puerto Ayacucho)'
        );

        $areas_id = array
        (
            0,
            8, 7, 7, 7, 7, 7,
            8, 7, 8, 8, 7, 7,
            9, 7, 9, 8, 8, 8,
            8, 8, 8, 8, 8, 8,
            8, 8
        );

        for($i = 1; $i < sizeof($codigos); $i++)
        {
            switch($areas_id[$i])
            {
                case 7:
                    $importa = $importates["Ciencias Económicas y Sociales"];
                    $mencion = $especialidades["Ciencias Económicas y Sociales"];
                    break;

                case 8:
                    $importa = $importates["Humanidades y Educación"];
                    $mencion = $especialidades["Humanidades y Educación"];
                    break;

                case 9:
                    $importa = $importates["Ciencias Juridicas y Politicas"];
                    $mencion = $especialidades["Ciencias Juridicas y Politicas"];
                    break;
            }

            \DB::table('carrera')->insert(
                array(
                    'id' => $id,
                    'codigo' => $codigos[$i],
                    'nombre' => $carerras[$i],
                    'facultad_id' => $areas_id[$i],
                    'materias_importantes' => $importa,
                    'menciones' => $mencion,
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
            $id++;
        }

/*
 *       \DB::table('carrera')->where('codigo', 10046)->update(["materias_importantes" => "1001,1005,1007,1008,1012,1009"]);//Geografía
        \DB::table('carrera')->where('codigo', 10050)->update(["materias_importantes" => "1005,1007,1008,1012,1009,1011"]);//Idiomas modernos
*/
        /*
         * Se agrega en la base de datos las carreras asociadas al area Ciencias del Agro y del Mar
         */

        $codigos = array
        (
            0,
            10230, 10231
        );

        $carerras = array
        (
            'Lista',
            'Ingeniería Agronómica', 'Medicina Veterinaria'
        );

        $areas_id = array
        (
            0,
            10, 11
        );

        for($i = 1; $i < sizeof($codigos); $i++)
        {
            switch($areas_id[$i])
            {
                case 10:
                    $importa = $importates["Agronomía"];
                    $mencion = $especialidades["Agronomía"];
                    break;

                case 11:
                    $importa = $importates["Ciencias Veterinaria"];
                    $mencion = $especialidades["Ciencias Veterinaria"];
                    break;
            }

            \DB::table('carrera')->insert(
                array(
                    'id' => $id,
                    'codigo' => $codigos[$i],
                    'nombre' => $carerras[$i],
                    'facultad_id' => $areas_id[$i],
                    'materias_importantes' => $importa,
                    'menciones' => $mencion,
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
            $id++;
        }
    }
}
