<?php

use Illuminate\Database\Seeder;
use App\Models\User;

/**
 * Description of UsersTableSeeder
 *
 * @author Gary Romero
 */

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [
                'email' => 'admin@test.com',
                'firstname' => 'Admin',
                'lastname' => 'Test',
                'password' => '12345678',
            ],
            [
                'email' => 'user@test.com',
                'firstname' => 'User',
                'lastname' => 'Test',
                'password' => '12345678',
            ]
        ];

        foreach ($usuarios as $key => $item) {
            $usuario = User::create([
                'firstname' => $item['firstname'],
                'lastname' => $item['lastname'],
                'email' => $item['email'],
                'password' => hash(env('ENCRYPTION_ALGORITHM'), $item['password']),
            ]);
            $usuario->save();
        }
    }
}
