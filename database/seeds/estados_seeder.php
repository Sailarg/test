<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class estados_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $estados=array('Amazonas','Anzoátegui','Apure','Aragua','Barinas','Bolívar','Carabobo','Cojedes','Delta Amacuro',
                        'Falcón','Guárico','Lara','Mérida','Miranda','Monagas','Nueva Esparta','Portuguesa','Sucre','Táchira',
                            'Trujillo','Vargas','Yaracuy','Zulia','Distrito Capital','Dependencias Federales');


        for($i = 0; $i < sizeof($estados); $i++)
        {
            \DB::table('estados')->insert(
                array(
                    'id' => $i+1,
                    'nombre' => $estados[$i],
                    'created_at' => $faker->dateTimeBetween($startDate = 'now', $endDate = 'now'),
                )
            );
        }

    }


}
