<?php

use Illuminate\Database\Seeder;
use App\Models\Priority;
use Faker\Factory as Faker;

/**
 * Description of PrioritiesTableSeeder
 *
 * @author Gary Romero
 */

class PrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index)
        {
            $priority = Priority::create([
                'name' =>  $faker->randomElement(array_keys(trans('generals.types'))),
            ]);
            $priority ->save();
        }
    }
}
