<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Priority;
use App\Models\Task;

/**
 * Description of TasksTableSeeder
 *
 * @author Gary Romero
 */

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,5) as $index) {
            $task = Task::create([
                'user_id' => User::select(['id'])->orderByRaw('RAND()')->first()->id,
                'priority_id' => Priority::select(['id'])->orderByRaw('RAND()')->first()->id,
                'title' => $faker->text(10),
                'description' => $faker->text(50),
                'due_date' => $faker->dateTime($max = 'now'),
            ]);
            $task->save();
        }
    }
}
