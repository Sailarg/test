<?php

namespace App\Models;

/**
 * Description of File
 *
 * @author Gary Romero
 */

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $table = 'files';

    protected $softDelete = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];



}
