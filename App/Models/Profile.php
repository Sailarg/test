<?php

namespace App\Models;

/**
 * Description of Profile
 *
 * @author Gary Romero
 */

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model
{
    use SoftDeletes;

    protected $table = 'profiles';

    protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'lastname', 'gender',
        'identity_card', 'birthday', 'country',
        'image_id'
    ];

    public function account()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function image()
    {
        return $this->belongsTo('App\Models\File');
    }

}
