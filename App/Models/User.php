<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use JWTAuth;

/**
 * Description of User
 *
 * @author Gary Romero
 */

class User extends Authenticatable
{
    use Notifiable;
    protected $table = 'users';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public static function isAuthenticate()
    {
        return JWTAuth::parseToken()->authenticate();
    }

    public static function authenticate($data)
    {
        $user = self::where('email', $data['email'])
            ->where('password', hash(env('ENCRYPTION_ALGORITHM'),$data['password']))->first();

        if($user) {
            $token = JWTAuth::fromUser($user);
            return ['token' => $token];
        };
        return false;
    }

}