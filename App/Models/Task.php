<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Task
 *
 * @author Gary Romero
 */

class Task extends Model
{
    protected $table = 'tasks';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'priority_id', 'title', 'description', 'due_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function priority()
    {
        return $this->belongsTo('App\Models\Priority');
    }
}
