<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Priority
 *
 * @author Gary Romero
 */

class Priority extends Model
{
    protected $table = 'priorities';

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

}
