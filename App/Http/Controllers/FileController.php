<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Validations\ValidationsFiles;
use Illuminate\Http\Request;

class FileController extends Controller
{

    public $timestamps = true;

    public function subir(Request $request)
    {
        if(isset($request['file']))
        {
            $file = $request->file('file');
            if ($request->hasFile('file'))
            {
                $destinationPath = '../storage/app/public';
                $originalName = str_replace(" ", "_", $file->getClientOriginalName());
                $fileName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $originalName) . "_" . str_random(50). '.' . $file->getClientOriginalExtension();// rename file
                $file->move($destinationPath, $fileName);
                $documento = new File();
                $documento->fill(['name' => $fileName]);

                if($documento->save())
                {
                    return response()->json(['data' => $documento->id], 200);
                }

            }

        }

        return response()->json(['data' => false, 'errors' => 'El archivo no pudo se cargado, intente de nuevo'], 200);
    }

    public function descargar(Request $request)
    {
        $data = $request->all();
        $validator = ValidationsFiles::donwload($data);

        if ($validator->fails())
        {
            return response()->json(['data' => false, 'errors' => $validator->messages()], 200);
        }

        $documento = File::where('id', $data['id'])->first();
        return response()->json(['data' => asset('storage/' . $documento['name'])], 200); // http://inet/storage/file
        //    return response()->json(['data' => Storage::url($documento['nombre'])], 200); // path /storage/file
    }


}
