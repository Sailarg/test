<?php

namespace App\Http\Controllers;

use App\Models\Priority;
use App\Validations\ValidationsPriority;
use Illuminate\Http\Request;

class PriorityController extends Controller
{

    public function show($id)
    {
        $validator = ValidationsPriority::show(['id' => $id]);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $priority = Priority::whereId($id)->get();

        if(!is_null($priority) || empty($priority))
        {
            $priority = $priority->filter(function ($p)
            {
                unset($p['id'], $p['created_at'], $p['updated_at']);
                return $p;
            });

            return response()->json(['response' => $priority], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function showAll()
    {
        $priority = Priority::all();

        if(!is_null($priority) || empty($priority))
        {
            $priority = $priority->filter(function ($p)
            {
                unset($p['id'], $p['created_at'], $p['updated_at']);
                return $p;
            });

            return response()->json(['response' => $priority], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = ValidationsPriority::store($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $priority = Priority::create($data);

        return ($priority->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function update(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsPriority::update($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $priority = Priority::whereId($data['id'])->first();
        $priority->fill($data);

        return ($priority->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function destroy(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsPriority::delete($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $priority = Priority::whereId($data['id']);

        if(!is_null($priority) || empty($priority))
        {
            $priority->delete();
            return response()->json(['response' => true], 200);
        }

        return response()->json(['response' => false], 200);
    }

}
