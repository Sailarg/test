<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Validations\ValidationsTask;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    public function show($id)
    {
        $validator = ValidationsTask::show(['id' => $id]);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $task = Task::with('user')->with('priority')->whereId($id)->get();

        if(!is_null($task) || empty($task))
        {
            return response()->json(['response' => $this->filter($task)], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function showAll()
    {
        $task = Task::with('user')->with('priority')->get();

        if(!is_null($task) || empty($task))
        {
            return response()->json(['response' => $this->filter($task)], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = ValidationsTask::store($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $task = Task::create($data);

        return ($task->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function update(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsTask::update($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $task = Task::whereId($data['id'])->first();
        $task->fill($data);

        return ($task->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function destroy(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsTask::delete($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $task = Task::whereId($data['id']);

        if(!is_null($task) || empty($task))
        {
            $task->delete();
            return response()->json(['response' => true], 200);
        }

        return response()->json(['response' => false], 200);
    }

    private function filter($tasks)
    {
        $tasks = $tasks->filter(function ($t)
        {

            unset($t['id'], $t['user_id'], $t['priority_id'], $t['user']['id'], $t['priority']['id'], $t['password'], $t['created_at'], $t['updated_at']);
            return $t;
        });

        return $tasks;
    }

}
