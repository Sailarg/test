<?php

namespace App\Http\Controllers;

use App\Models\Profile;
use App\Validations\ValidationsUsers;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function show($id)
    {
        $validator = ValidationsUsers::show(['id' => $id]);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $user = User::whereId($id)->get();

        if(!is_null($user) || empty($user))
        {
            return response()->json(['response' => $this->filter($user)], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function showAll()
    {
        $users = Profile::with('account')->get();

        if(!is_null($users) || empty($users))
        {
            return response()->json(['response' => $this->filter($users)], 200);
        }

        return response()->json(['response' => false], 200);
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $validator = ValidationsUsers::store($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $data['password'] = hash(env('ENCRYPTION_ALGORITHM'), $data['password']);
        $user = User::create($data);

        return ($user->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function update(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsUsers::update($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $data['password'] = hash(env('ENCRYPTION_ALGORITHM'), $data['password']);
        $priority = User::whereId($data['id'])->first();
        $priority->fill($data);

        return ($priority->save()) ? response()->json(['response' => true], 200) :
            response()->json(['response' => false], 404);
    }

    public function destroy(Request $request, $id)
    {
        $request->merge(['id' => $id]);
        $data = $request->all();
        $validator = ValidationsUsers::delete($data);

        if ($validator->fails())
        {
            return response()->json(['response' => false, 'errors' => $validator->messages()->first()], 404);
        }

        $priority = User::whereId($data['id']);

        if(!is_null($priority) || empty($priority))
        {
            $priority->delete();
            return response()->json(['response' => true], 200);
        }

        return response()->json(['response' => false], 200);
    }

    private function filter($user)
    {
        $user = $user->filter(function ($u)
        {
            unset($u['id'], $u['password'], $u['created_at'], $u['updated_at']);
            return $u;
        });

        return $user;
    }

}
