<?php
namespace App\Http\Controllers\Auth;

use App\Models\User;
use Auth;
use JWTAuth;
use Illuminate\Http\Response as HttpResponse;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    /**
    *   Rules Login
    **/
    private $rules = [
        'email' => 'required|email',
        'password' => 'required',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['logout']]);
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['response' => true], 200);
    }

    public function authenticate(Request $request) {

        try
        {
            $credentials = [
                'email' => $request->get('email'),
                'password' => $request->get('password'),
            ];

            if (!$usuario = User::authenticate($credentials))
            {
                return response()->json(false, HttpResponse::HTTP_UNAUTHORIZED);
            }

            return response()->json(compact('usuario'));
        }
        catch (JWTException $e)
        {
            return response()->json(['errors' => 'could_not_create_token'], 404);
        }
    }

    public function finish(Request $request) {

        try {
            $this->validate($request, [
                'token' => 'required' 
            ]);

            JWTAuth::invalidate($request->input('token'));

        }
        catch (JWTException $e)
        {
            return response()->json(['errors' => 'token_invalid'], 404);
        }

        return response()->json(['message' => 'OK'], 200);
    }

}