<?php

namespace App\Http\Middleware;

use App\Modelos\Usuario;
use App\Models\User;
use Closure;

class Activo
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $usuario = User::isAuthenticate();

        if ($usuario)
        {
            return $next($request);
        }

        return response()->json(['response' => false, 'errors' => 'Unauthorized User'], 404);

    }

}
