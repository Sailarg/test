<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

class ValidationsUsers
{

    public static function show($data)
    {
        $rules = [
            'id' => 'required|exists:users,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function store($data)
    {
        $rules = [
            'firstname' => 'required|string',
            'lastname'=> 'required|string',
            'password'=> 'required|string',
            'email' => 'required|email|unique:users',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function update($data)
    {
        $rules = [
            'id' => 'required|exists:users,id',
            'firstname' => 'string',
            'lastname'=> 'string',
            'password'=> 'string',
            'email' => 'email|unique:users',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function delete($data)
    {
        $rules = [
            'id' => 'required|exists:users,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

}
