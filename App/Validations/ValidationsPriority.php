<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

class ValidationsPriority
{

    public static function show($data)
    {
        $rules = [
            'id' => 'required|exists:priorities,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function store($data)
    {
        $rules = [
            'name' => 'required|string',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }
    
    public static function update($data)
    {
        $rules = [
            'id' => 'required|integer|exists:priorities,id',
            'name' => 'required|string',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function delete($data)
    {
        $rules = [
            'id' => 'required|integer|exists:priorities,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

}
