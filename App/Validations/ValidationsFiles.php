<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

/**
 * Description of ValidationsFiles
 *
 * @author Gary Romero
 */
class ValidationsFiles
{

    public static function donwload($data)
    {
        $rules = [
            'id' => 'required|integer|exists:files,id',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

}
