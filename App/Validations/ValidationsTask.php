<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Validations;

use Illuminate\Support\Facades\Validator;

class ValidationsTask
{

    public static function show($data)
    {
        $rules = [
            'id' => 'required|exists:tasks,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function store($data)
    {
        $rules = [
            'user_id'  => 'required|exists:users,id',
            'priority_id' => 'required|exists:priorities,id',
            'title'  => 'required|string',
            'description'  => 'required|string',
            'due_date' => 'required|date_format:Y-m-d H:i:s',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function update($data)
    {
        $rules = [
            'id' => 'required|exists:tasks,id',
            'user_id'  => 'exists:users,id',
            'priority_id' => 'exists:priorities,id',
            'title'  => 'string',
            'description'  => 'string',
            'due_date' => 'date',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function delete($data)
    {
        $rules = [
            'id' => 'required|exists:tasks,id',
        ];
        $validator = Validator::make($data, $rules);
        return $validator;
    }

}
